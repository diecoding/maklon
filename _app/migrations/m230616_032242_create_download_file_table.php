<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%download_file}}`.
 */
class m230616_032242_create_download_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%download_file}}', [
            'id' => $this->primaryKey(),

            'nama'       => $this->string()->notNull(),
            'email'      => $this->string(),
            'hp'         => $this->string(),
            'keterangan' => $this->string(1024),

            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%download_file}}');
    }
}

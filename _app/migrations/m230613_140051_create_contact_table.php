<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contact}}`.
 */
class m230613_140051_create_contact_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%contact}}', [
            'id' => $this->primaryKey(),

            'nama'               => $this->string()->notNull(),
            'jenis_kelamin'      => $this->string(32),
            'tanggal_berkunjung' => $this->date(),
            'email'              => $this->string(),
            'hp'                 => $this->string(),
            'domisili'           => $this->string(512),
            'budget'             => $this->string(),
            'keterangan'         => $this->string(1024),

            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%contact}}');
    }
}

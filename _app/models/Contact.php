<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%contact}}".
 *
 * @property int $id
 * @property string $nama
 * @property string|null $jenis_kelamin
 * @property string|null $tanggal_berkunjung
 * @property string|null $email
 * @property string|null $hp
 * @property string|null $domisili
 * @property string|null $budget
 * @property string|null $keterangan
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%contact}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [];
        $behaviors[] = [
            'class' => TimestampBehavior::class,
            'value' => date('Y-m-d H:i:s'),
        ];

        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['tanggal_berkunjung', 'created_at', 'updated_at'], 'safe'],
            [['nama', 'email', 'hp', 'budget'], 'string', 'max' => 255],
            [['jenis_kelamin'], 'string', 'max' => 32],
            [['domisili'], 'string', 'max' => 512],
            [['keterangan'], 'string', 'max' => 1024],
            [['email'], 'email'],
            ['jenis_kelamin', 'in', 'range' => ['Laki-laki', 'Perempuan']],
            ['budget', 'in', 'range' => [
                '100 - 250 Juta',
                '250 - 500 Juta',
                '> 500 Juta',
            ]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('app', 'ID'),
            'nama'               => Yii::t('app', 'Nama'),
            'jenis_kelamin'      => Yii::t('app', 'Jenis Kelamin'),
            'tanggal_berkunjung' => Yii::t('app', 'Tanggal Berkunjung'),
            'email'              => Yii::t('app', 'Email'),
            'hp'                 => Yii::t('app', 'No. Telepon/HP'),
            'domisili'           => Yii::t('app', 'Domisili'),
            'budget'             => Yii::t('app', 'Budget'),
            'keterangan'         => Yii::t('app', 'Keterangan'),
            'created_at'         => Yii::t('app', 'Tanggal Dibuat'),
            'updated_at'         => Yii::t('app', 'Tanggal Diubah'),
        ];
    }
}

<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%download_file}}".
 *
 * @property int $id
 * @property string $nama
 * @property string|null $email
 * @property string|null $hp
 * @property string|null $keterangan
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class DownloadFile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%download_file}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [];
        $behaviors[] = [
            'class' => TimestampBehavior::class,
            'value' => date('Y-m-d H:i:s'),
        ];

        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama', 'email', 'hp'], 'string', 'max' => 255],
            [['keterangan'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('app', 'ID'),
            'nama'           => Yii::t('app', 'Nama'),
            'email'          => Yii::t('app', 'Email'),
            'hp'             => Yii::t('app', 'No. Telepon/HP'),
            'keterangan'     => Yii::t('app', 'Keterangan'),
            'created_at'     => Yii::t('app', 'Tanggal Dibuat'),
            'updated_at'     => Yii::t('app', 'Tanggal Diubah'),
        ];
    }
}

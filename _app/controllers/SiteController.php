<?php

namespace app\controllers;

use app\models\Contact;
use app\models\DownloadFile;
use Yii;
use yii\helpers\StringHelper;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     */
    public function actionTentang()
    {
        
        $model = new DownloadFile();
        
        if ($model->load($this->request->post())) {
            
            if ($model->save()) {
                return Yii::$app->getResponse()->sendFile(dirname(__DIR__) . '/donwloads/test.pdf', 'ComPro ARN');
            } else {
                Yii::$app->session->setFlash('error', 'Permintaan download file gagal. Mohon teliti kembali form Anda.');
            }
        }

        return $this->render('tentang', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionProsesMaklon()
    {
        return $this->render('proses-maklon');
    }

    /**
     * @return string
     */
    public function actionSertifikasi()
    {
        return $this->render('sertifikasi');
    }

    /**
     * @return string
     */
    public function actionOem()
    {
        return $this->render('oem');
    }

    /**
     * @return string
     */
    public function actionOdm()
    {
        return $this->render('odm');
    }

    /**
     * @return string
     */
    public function actionKonsultasiProduk()
    {
        return $this->render('konsultasi-produk');
    }
    
    /**
     * @return string
     */
    public function actionResearchDevelopment()
    {
        return $this->render('research-development');
    }
    
    /**
     * @return string
     */
    public function actionMerekPaten()
    {
        return $this->render('merek-paten');
    }

    /**
     * @return string
     */
    public function actionFaq()
    {
        return $this->render('faq');
    }

    /**
     * @return string
     */
    public function actionProduk()
    {
        return $this->render('produk');
    }

    /**
     * @return string
     */
    public function actionBlog()
    {
        return $this->render('blog');
    }

    /**
     * @return string
     */
    public function actionFaqIzinBpom()
    {
        return $this->render('faq-izin-bpom');
    }
    public function actionFaqIzinHki()
    {
        return $this->render('faq-izin-hki');
    }
    public function actionFaqIzinHalal()
    {
        return $this->render('faq-izin-halal');
    }

    /**
     * @return string
     */
    public function actionKontakKami()
    {
        $model = new Contact();
        
        if ($model->load($this->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Jadwal kunjungan berhasil dikirimkan.');

                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', 'Jadwal kunjungan gagal dikirimkan. Mohon teliti kembali form Anda.');
            }
        }

        return $this->render('kontak-kami', [
            'model' => $model,
        ]);
    }
}

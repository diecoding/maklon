<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use yii\bootstrap4\Html;
use yii\helpers\Url;

AppAsset::register($this);

// $this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
// $this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
// $this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
// $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);

$title = $this->title ? $this->title . ' - Maklon Nutrisi & Beauty' : 'Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara';

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <title><?= Html::encode($title) ?></title>

    <meta name="description" content="PT Alga Rosan Nusantara" />
    <meta name="keywords" content="Maklon, Nutrisi, Beauty" />
    <meta name="author" content="Maklon Nutrisi & Beauty" />

    <!-- favicon -->
    <link rel="shortcut icon" href="<?= Yii::$app->homeUrl ?>favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?= Yii::$app->homeUrl ?>web/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= Yii::$app->homeUrl ?>web/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= Yii::$app->homeUrl ?>web/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= Yii::$app->homeUrl ?>web/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= Yii::$app->homeUrl ?>web/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= Yii::$app->homeUrl ?>web/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= Yii::$app->homeUrl ?>web/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= Yii::$app->homeUrl ?>web/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::$app->homeUrl ?>web/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= Yii::$app->homeUrl ?>web/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::$app->homeUrl ?>web/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= Yii::$app->homeUrl ?>web/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::$app->homeUrl ?>web/favicon-16x16.png">
    <link rel="manifest" href="<?= Yii::$app->homeUrl ?>web/manifest.json">
    <meta name="msapplication-TileColor" content="#1b9c85">
    <meta name="msapplication-TileImage" content="<?= Yii::$app->homeUrl ?>web/ms-icon-144x144.png">
    <meta name="theme-color" content="#1b9c85">
    <meta property="og:image" content="<?= Yii::$app->homeUrl ?>web/og.png" />

    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>

    <div id="wrapper">
        <div id="content">
            <!-- Start header -->
            <header class="header-nav-center active-blue" id="myNavbar" style="border-bottom: 2px solid rgba(0, 0, 0, 0.1)">
                <div class="container">
                    <!-- navbar -->

                    <?= $this->render('menu') ?>

                    <!-- End Navbar -->
                </div>
                <!-- end container -->
            </header>
            <!-- End header -->

            <!-- Stat main -->
            <main data-spy="scroll" data-target="#navbar-example2" data-offset="0">

                <?= $content ?>

            </main>
            <!-- end main -->

            <?= $this->render('footer') ?>

        </div>

        <!-- Start Section Loader -->
        <section class="loading_overlay d-none">
            <div class="loader_logo">
                <img class="logo" src="<?= Yii::$app->homeUrl ?>web/loader.png" />
            </div>
        </section>
        <!-- End. Loader -->
    </div>
    <!-- End. wrapper -->

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
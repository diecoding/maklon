<?php

use yii\helpers\Url;

?>

<nav class="navbar navbar-expand-lg navbar-light px-sm-0">
    <a class="navbar-brand mr-1" href="<?= Yii::$app->homeUrl ?>">
        <img class="logo" src="<?= Yii::$app->homeUrl ?>web/logo.png" alt="" />
    </a>

    <button class="navbar-toggler menu ripplemenu" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <svg viewBox="0 0 64 48">
            <path d="M19,15 L45,15 C70,15 58,-2 49.0177126,7 L19,37"></path>
            <path d="M19,24 L45,24 C61.2371586,24 57,49 41,33 L32,24"></path>
            <path d="M45,33 L19,33 C-8,33 6,-2 22,14 L45,37"></path>
        </svg>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto nav-pills">
            <li class="nav-item">
                <a class="nav-link" href="<?= Yii::$app->homeUrl ?>">Home</a>
            </li>

            <li class="nav-item dropdown dropdown-hover">
                <a class="nav-link dropdown-toggle dropdown_menu" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    About

                    <div class="icon_arrow">
                        <i class="tio chevron_right"></i>
                    </div>

                </a>
                <div class="dropdown-menu single-drop sm_dropdown">
                    <ul class="dropdown_menu_nav">

                        <li>
                            <a class="dropdown-item" href="<?= Url::toRoute('tentang') ?>">Tentang Kami</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="<?= Url::toRoute('proses-maklon') ?>">Proses Maklon</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="<?= Url::toRoute('sertifikasi') ?>">Sertifikasi</a>
                        </li>

                    </ul>

                </div>
            </li>

            <li class="nav-item dropdown dropdown-hover">
                <a class="nav-link dropdown-toggle dropdown_menu" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Service

                    <div class="icon_arrow">
                        <i class="tio chevron_right"></i>
                    </div>

                </a>
                <div class="dropdown-menu single-drop sm_dropdown">
                    <ul class="dropdown_menu_nav">

                        <li>
                            <a class="dropdown-item" href="<?= Url::toRoute('oem') ?>">OEM (Original Equipment Manufacture)</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="<?= Url::toRoute('odm') ?>">ODM (Original Design Manufacture)</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="<?= Url::toRoute('konsultasi-produk') ?>">Konsultasi Produk</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="<?= Url::toRoute('research-development') ?>">Research & Development</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="<?= Url::toRoute('merek-paten') ?>">Merek & Paten</a>
                        </li>

                    </ul>

                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="<?= Url::toRoute('produk') ?>">Produk</a>
            </li>

            <li class="nav-item dropdown dropdown-hover">
                <a class="nav-link dropdown-toggle dropdown_menu" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ARN Media

                    <div class="icon_arrow">
                        <i class="tio chevron_right"></i>
                    </div>

                </a>
                <div class="dropdown-menu single-drop sm_dropdown">
                    <ul class="dropdown_menu_nav">

                        <li>
                            <a class="dropdown-item" href="<?= Url::toRoute('blog') ?>">Blog</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="<?= Url::toRoute('faq') ?>">FAQ</a>
                        </li>

                    </ul>

                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="<?= Url::toRoute('kontak-kami') ?>">Kontak Kami</a>
            </li>
        </ul>
    </div>
</nav>
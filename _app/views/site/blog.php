<?php

/** @var yii\web\View $this */

use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Blog';

?>


<!-- Start banner_about -->
<section class="pt_banner_inner banner_px_image">
    <div class="parallax_cover">
        <img class="cover-parallax h-100vh" src="<?= Yii::$app->homeUrl ?>web/maklon/header.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-12">
                <div class="banner_title_inner c-white">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        Blog
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100">
                        PT Alga Rosan Nusantara selain didukung oleh
                        Revolusi Digital dengan memanfaatkan platform Sosmed untuk mengoptimalkan pemasaran Produk juga didukung oleh Konsep Karnus. Konsep kesehatan sesuai dengan Algoritma Sang Pencipta sehingga tidak menyimpang dari dunia kesehatan saat ini.
                    </p>
                    <div data-aos="fade-up" data-aos-delay="200">
                        <a href="https://konsepkarnus.com/" class="btn btn_sm_primary bg-orange-red c-white rounded-8">Konsep Karnus</a>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>
<!-- End banner_about -->
<section class="section__showcase margin-t-6">
    <div class="container">
        <div class="block__tab">
            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="pills-skincare-tab" data-toggle="pill" href="#pills-skincare" role="tab" aria-controls="pills-skincare" aria-selected="true">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="10.279" height="19" viewBox="0 0 10.279 19">
                                <g id="Group_6271" data-name="Group 6271" transform="translate(-7.285 -2)">
                                    <rect id="Rectangle" width="2" height="4" rx="1" transform="translate(11.5 2)" fill="#0b2238" opacity="0.3" />
                                    <rect id="Rectangle-Copy-3" width="2" height="5" rx="1" transform="translate(11.5 16)" fill="#0b2238" opacity="0.3" />
                                    <path id="Combined-Shape" d="M15.493,8.044A2.933,2.933,0,0,0,14.4,7.2a3.082,3.082,0,0,0-1.3-.313,3.875,3.875,0,0,0-.656.057,1.692,1.692,0,0,0-.6.219,1.48,1.48,0,0,0-.446.418,1.113,1.113,0,0,0-.181.656,1.057,1.057,0,0,0,.142.57,1.268,1.268,0,0,0,.418.4,3.726,3.726,0,0,0,.656.314q.38.143.855.294.684.228,1.425.5a5.329,5.329,0,0,1,1.349.731,3.84,3.84,0,0,1,1.007,1.131,3.256,3.256,0,0,1,.4,1.681,4.393,4.393,0,0,1-.427,2,4,4,0,0,1-1.149,1.4,4.926,4.926,0,0,1-1.653.817,6.951,6.951,0,0,1-1.919.266,7.924,7.924,0,0,1-2.793-.5A5.917,5.917,0,0,1,7.285,16.4l2.128-2.166a3.932,3.932,0,0,0,1.3,1.017,3.532,3.532,0,0,0,1.605.408,3.231,3.231,0,0,0,.7-.076,1.754,1.754,0,0,0,.6-.247,1.237,1.237,0,0,0,.408-.456,1.435,1.435,0,0,0,.152-.684,1.082,1.082,0,0,0-.19-.646,1.856,1.856,0,0,0-.542-.484,4.361,4.361,0,0,0-.874-.4q-.523-.181-1.187-.389a11.348,11.348,0,0,1-1.264-.494,4.229,4.229,0,0,1-1.1-.731,3.388,3.388,0,0,1-.779-1.083A3.671,3.671,0,0,1,7.95,8.424a3.808,3.808,0,0,1,.456-1.919A3.964,3.964,0,0,1,9.6,5.194a5.142,5.142,0,0,1,1.672-.75,7.581,7.581,0,0,1,1.881-.238,6.983,6.983,0,0,1,2.327.418,5.993,5.993,0,0,1,2.08,1.235Z" fill="#0b2238" fill-rule="evenodd" />
                                </g>
                            </svg>
                        </div>
                        <h3>Skincare</h3>
                        <div class="prog"></div>
                    </a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-beauty-tab" data-toggle="pill" href="#pills-beauty" role="tab" aria-controls="pills-beauty" aria-selected="false">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="17.06" height="17.06" viewBox="0 0 17.06 17.06">
                                <g id="Group_6271" data-name="Group 6271" transform="translate(-3.343 -3.597)">
                                    <path id="Combined-Shape" d="M15.95,3.808,13.025,6.733a2,2,0,0,0,0,2.828l1.414,1.414a2,2,0,0,0,2.828,0L20.192,8.05a5.92,5.92,0,0,1-8.308,6.894L6.757,20.071a2,2,0,1,1-2.828-2.828l5.127-5.127A5.92,5.92,0,0,1,15.95,3.808Z" fill="#0b2238" fill-rule="evenodd" />
                                    <path id="Rectangle-2" d="M16.657,5.929l1.414,1.414a1,1,0,0,1,0,1.414l-1.38,1.38a1,1,0,0,1-1.414,0L13.863,8.723a1,1,0,0,1,0-1.414l1.38-1.38A1,1,0,0,1,16.657,5.929Z" fill="#0b2238" fill-rule="evenodd" opacity="0.3" />
                                </g>
                            </svg>
                        </div>
                        <h3>Beauty Tips</h3>
                        <div class="prog"></div>
                    </a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-kosmetik-tab" data-toggle="pill" href="#pills-kosmetik" role="tab" aria-controls="pills-kosmetik" aria-selected="false">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16.94" height="17" viewBox="0 0 16.94 17">
                                <g id="Group_6274" data-name="Group 6274" transform="translate(-3.06 -3)">
                                    <path id="Combined-Shape" d="M4,12.2,13,14V4.062A8,8,0,1,1,4,12.2Z" fill-rule="evenodd" opacity="0.3" />
                                    <path id="Combined-Shape-Copy" d="M3.06,10.012A8,8,0,0,1,11,3v8.6Z" fill-rule="evenodd" />
                                </g>
                            </svg>
                        </div>
                        <h3>Kosmetik</h3>
                        <div class="prog"></div>
                    </a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-maklon-tab" data-toggle="pill" href="#pills-maklon" role="tab" aria-controls="pills-maklon" aria-selected="false">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="18" viewBox="0 0 16 18">
                                <g id="Group_6273" data-name="Group 6273" transform="translate(-4 -3)">
                                    <path id="Rectangle" d="M6,3H18l2,3.5H4Z" fill="#0b2238" fill-rule="evenodd" opacity="0.3" />
                                    <path id="Combined-Shape" d="M6,5H18a2,2,0,0,1,2,2V19a2,2,0,0,1-2,2H6a2,2,0,0,1-2-2V7A2,2,0,0,1,6,5ZM9,9a1,1,0,0,0,0,2h6a1,1,0,0,0,0-2Z" fill="#0b2238" fill-rule="evenodd" />
                                </g>
                            </svg>
                        </div>
                        <h3>Maklon Kosmetik</h3>
                        <div class="prog"></div>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-skincare" role="tabpanel" aria-labelledby="pills-skincare-tab">
            <!-- skincare -->
            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Pentingnya Rutinitas Perawatan Kulit dan Pengaruhnya Terhadap Kesehatan
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Kulit adalah bagian terluar dari tubuh manusia, yang berfungsi sebagai
                                    lapisan pelindung yang membantu mengatur suhu tubuh, melindungi dari
                                    zat berbahaya dan mengatur fungsi otak manusia.
                                </p>
                                <p>
                                    Oleh karena itu, kita tidak dapat mengabaikan rutinitas perawatan kulit
                                    kita, karena dapat menyebabkan penuaan dini dan masalah kulit lainnya
                                    seperti jerawat, eksim, psoriasis, atau rosacea.
                                </p>
                                <p>
                                    Mempertahankan rutinitas perawatan kulit yang sehat sangat penting
                                    untuk menjaga kulit kamu terlihat sehat dan bercahaya. Produk dengan
                                    sifat melembabkan dan menyejukkan sangat dianjurkan untuk menjaga
                                    kesehatan kulit dan memperbaiki penampilan kulit.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Berapa Usia Terbaik Untuk Mulai Pakai Produk Skincare?
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Kapan Remaja Boleh Menggunakan Produk Skincare? Rahasia rutinitas
                                    perawatan kulit yang baik adalah selama masa remaja kamu.
                                    Produk perawatan kulit atau skincare penting untuk kamu gunakan dimulai
                                    di usia remaja. Manfaatnya akan terasa saat kamu menginjak usia 20an.
                                </p>
                                <p>
                                    Skincare menjadi topik yang hangat diperbincangkan di media sosial.
                                </p>
                                <p>
                                    Konten tentang skincare pun banyak dikonsumsi oleh anak-anak sekolah
                                    masa kini. Ketertarikan menggunakan produk skincare pun mulai tertanam
                                    di benak mereka. Tapi tahukah kamu kapan waktu yang tepat untuk mulai
                                    mengenakan produk perawatan kulit alias skincare?
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Tren Skincare Dengan Bahan Natural Untuk Pria Tahun 2022 [Men's Edition]
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Yuk kita jujur, kebanyakan dari kamu kalau menggunakan produk, carinya yang
                                    sederhana dan gampang dipakai, betul bukan? Seperti contohnya sabun muka
                                    saja bisa menggunakan sabun batangan untuk membersihkan wajah. Akan
                                    tetapi kalau kita bisa teliti lebih dalam, wajah kita itu ada jenis nya dan
                                    penyesuaiannya. Justru ini yang mesti dikupas lebih lanjut!
                                </p>
                                <p>Kabar baiknya adalah bahwa kita tidak perlu se”teori” itu untuk menggunakan
                                    skincare, apalagi untuk laki-laki. Dimulai dari menggunakan body lotion, face
                                    cleanser, dan moisturizer sudah lebih dari cukup untuk pria bisa dikategorikan
                                    “peduli” kepada kesehatan kulit, dimana pada umumnya mungkin kebanyakan
                                    tidak seperti itu.
                                </p>
                                <p>Jadi, apa sih kira-kira skincare alami terbaik untuk para pria? Pertanyaan yang
                                    bagus! Artinya tidak ada bahan kimia yang berbahaya yang ditambahkan
                                    tentunya.
                                </p>
                                <p>Berikut contoh pembersih wajah alami terbaik untuk pria di tahun 2022, yang
                                    semuanya akan membuat wajah kalian para pria merasa bahagia, terhidrasi,
                                    percaya diri, dan yang terpenting: Bebas Kerut.
                                </p>
                                <p>Tipe kulit itu benar-benar bervariasi, banyak skincare yang beredar di pasaran
                                    justru lebih banyak merugikan daripada cocok dan baik untuk wajah Anda,
                                    dengan cara menghilangkan semua lapisan kulit (mati atau hidup) dari wajah
                                    Anda.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    10 langkah Konsep Karnus untuk Kulit Glowing Sehat
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Menyusul
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Manfaat dari Ashitaba dan Collagen untuk Kecantikan & Kesehatan Kulit
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Menyusul
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Sinar Ultraviolet: Pengertian, Jenis, Manfaat, dan Bahayanya pada Kesehatan
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Menyusul
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Ayo Kenali Jenis Moisturizer dan Manfaatnya bagi Kulit Wajah Kamu
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Menyusul
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="tab-pane fade" id="pills-beauty" role="tabpanel" aria-labelledby="pills-beauty-tab">
            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Apakah Penting Menjaga Skin Barrier? Tips Mengenali Tanda Kondisi Kulitmu & Cara Memperbaikinya
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Menyusul
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    6 Produk Wajib yang Harus Dipakai untuk Menjaga Kesehatan Kulit
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Menyusul
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Waspada Ini 9 Ciri Krim Pemutih Wajah Berbahaya Menurut BPOM
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Menyusul
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Kenali 7 Manfaat Tree Tea Oil untuk Kecantikan Kulit
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Menyusul
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="tab-pane fade" id="pills-kosmetik" role="tabpanel" aria-labelledby="pills-kosmetik-tab">
            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Ini Dia Perbedaan Skincare dan Make Up yang Perlu Kamu Ketahui
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Menyusul
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Bisa Mengurangi Stres, Berikut Manfaat Parfum Bagi Kesehatan!
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Menyusul
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <div class="tab-pane fade" id="pills-maklon" role="tabpanel" aria-labelledby="pills-maklon-tab">
            <section class="pt_banner_inner padding-t-2">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-12">
                            <div class="title_sections_inner margin-b-5">
                                <h5 class="c-blue">(Miliki Brand Skin Care-mu Sendiri Bersama PT.
                                    Alga Rosan Nusantara: Your Best Bisnis
                                    Partner!)
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Peluang bisnis skincare di era digital saat ini
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Perkembangan industri kosmetik di Indonesia mengalami pertumbuhan yang cukup
                                    pesat, hal itu karena tingginya permintaan konsumen dan meluasnya pasar kosmetik.
                                    Maka jangan sia-siakan kesempatan bagus ini. Kami menerima maklon kosmetik
                                    selalu melakukan inovasi demi terciptanya produk - produk yang bermanfaat tinggi.
                                    Bukan hanya karena di back up oleh Konsep Karnus dengan tagline "Cara Cerdas
                                    Untuk Sehat”, namun kami juga memastikan semua bahan aktifnya sangat aman.
                                    Brand - brand yang telah bekerja sama dengan kami sudah terbukti diminati
                                    konsumen yang menginginkan cantik namun dengan cara yang sehat. PT. Alga Rosan
                                    Nusantara terus membuka peluang kepada pebisnis muda yang ingin memulai bisnis
                                    di dunia skin care dan kosmetik.
                                    Bersama kami, anda yang ingin menjadi pebisnis kosmetik tidaklah rumit. Siapapun
                                    bisa menjadi bagian dari beautypreneur asalkan memiliki keinginan yang kuat untuk
                                    membuat bisnis kecantikan.
                                    Caranya mudah, hubungi kami untuk bertemu langsung membahas inovasi produk
                                    yang akan dibuat.
                                    Inovasi yang selalu terus kami kembangkan adalah memanfaatkan sumber daya alam
                                    Indonesia sebagai salah satu bahannya. Yaitu Bovine Collagen yang mana pabriknya
                                    kami miliki sendiri dan menjadi satu - satunya pabrik kolagen sapi halal pertama di
                                    Asia Tenggara.
                                    Meskipun saat ini semakin banyak perusahaan maklon di Indonesia, Kami tetap
                                    konsisten untuk terus melakukan inovasi baru agar terciptanya sebuah produk
                                    berkualitas dan diminati oleh banyak konsumen tanpa adanya manipulasi apapun.
                                    Share This Article
                                    logo: FB IG WA Twitter
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Apa itu Jasa Maklon? Cari Tahu di Sini!
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Semakin banyak brand dan produk lokal berlomba meluncurkan seri produk mereka di
                                    market dan mendapat sambutan yang sangat baik sehingga semakin banyak pebisnis
                                    maupun pemula yang tertarik untuk mencoba terjun ke bisnis ini. Tapi berapa ya
                                    modal yang harus dikeluarkan untuk memulai bisnis ini, apa perlu memiliki pabrik
                                    sendiri?
                                    Jawabannya adalah tidak!
                                    Anda tidak perlu memiliki pabrik sendiri untuk membangun bisnis kosmetik dan skin
                                    care. Anda dapat bekerja sama dengan perusahaan maklon untuk membantu proses
                                    pembuatannya, dari proses pembuatan konsep produk hingga produksinya nanti.
                                    Modal awal untuk membuat produk kecantikan dengan brand milik Anda sendiri
                                    tidaklah membutuhkan biaya yang sangat besar. Lantas, berapakah biaya yang
                                    dibutuhkan untuk maklon kosmetik? Apa saja biaya yang dibutuhkan?
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="pt_banner_inner banner_Sblog_default padding-t-8">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-md-8 col-lg-7 my-auto">
                            <div class="banner_title_inner margin-b-8">
                                <h2>
                                    Apa itu Jasa Maklon? Cari Tahu di Sini!
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover_Sblog">
                                <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-Sblog mb-5" data-sticky-container>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                                <div class="item">
                                    <div class="profile_user">
                                        <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                        <div class="txt">
                                            <h4>
                                                Chad Faircloth
                                            </h4>
                                            <time>30 Sep, 2020</time>
                                        </div>
                                        <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                            <div class="inside_item">
                                                <span data-hover="Profile">Profile</span>
                                                <span></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="share_socail">
                                    <div class="title">Share</div>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio facebook"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio twitter"></i>
                                    </button>
                                    <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                        <i class="tio whatsapp_outlined"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="body_content">
                                <p class="margin-b-3">Anda ingin menjadi seorang pebisnis
                                    kosmetik & skincare namun bingung harus
                                    memikirkan modal untuk membangun pabrik
                                    dan membeli peralatan produksi?
                                    Belum Anda harus membayar upah tenaga
                                    kerja, strategi pemasarannya dll?
                                    Caranya Anda bisa menggunakan jasa maklon.
                                    Apa itu jasa maklon?
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>


<?php // for ($i = 0; $i < 10; $i++) { 
?>
<!-- <section class="pt_banner_inner banner_Sblog_default padding-t-8">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-md-8 col-lg-7 my-auto">
                    <div class="banner_title_inner margin-b-8">
                        <h1>
                            Pandemic crippling small-scale fishing worldwide, study finds
                        </h1>
                    </div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb default justify-content-center">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Blog</a></li>
                            <li class="breadcrumb-item active" aria-current="page">How under-screen cameras could change UI for
                                the better</li>
                        </ol>
                    </nav>

                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="cover_Sblog">
                        <img class="cover-parallax" src="<?= Yii::$app->homeUrl ?>web/img/inner/04984.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content-Sblog mb-5" data-sticky-container>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="fixSide_scroll" data-sticky-for="1023" data-margin-top="100">
                        <div class="item">
                            <div class="profile_user">
                                <img src="<?= Yii::$app->homeUrl ?>web/img/persons/26.jpg" alt="">
                                <div class="txt">
                                    <h4>
                                        Chad Faircloth
                                    </h4>
                                    <time>30 Sep, 2020</time>
                                </div>
                                <a href="#" class="btn btn_profile c-white sweep_top sweep_letter rounded-pill bg-lightgreen">
                                    <div class="inside_item">
                                        <span data-hover="Profile">Profile</span>
                                        <span></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="share_socail">
                            <div class="title">Share</div>

                            <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="https://ellisonleao.github.io/sharer.js/">
                                <i class="tio facebook"></i>
                            </button>

                            <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Twitter" data-sharer="twitter" data-title="Checkout Sharer.js!" data-hashtags="awesome, sharer.js" data-url="https://ellisonleao.github.io/sharer.js/">
                                <i class="tio twitter"></i>
                            </button>

                            <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Whatsapp" data-sharer="whatsapp" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/">
                                <i class="tio whatsapp_outlined"></i>
                            </button>

                            <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Telegram" data-sharer="telegram" data-title="Checkout Sharer.js!" data-url="https://ellisonleao.github.io/sharer.js/" data-to="+44555-03564">
                                <i class="tio telegram"></i>
                            </button>

                            <button class="btn icon" data-toggle="tooltip" data-placement="right" title="Pinterest" data-sharer="pinterest" data-url="https://ellisonleao.github.io/sharer.js/">
                                <i class="tio pinterest_circle"></i>
                            </button>

                            <button class="btn icon" data-toggle="tooltip" data-placement="right" title="skype" data-sharer="skype" data-url="https://ellisonleao.github.io/sharer.js/" data-title="Checkout Sharer.js!">
                                <i class="tio skype"></i>
                            </button>

                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="body_content">
                        <p class="margin-b-3">Many people don’t really know the difference between software architecture and
                            software design.
                            Even for developers, the line is often blurry and they might mix up elements of software
                            architecture patterns and design patterns. </p>
                        <h3>The Definition of Software Architecture</h3>
                        <p class="margin-b-3">In simple words, software architecture is the process of converting software
                            characteristics such
                            as flexibility, scalability, feasibility, reusability, and <mark>security into a structured
                                solution</mark> that
                            meets the technical and the business expectations. </p>
                        <h3>The Characteristics of Software Architecture</h3>
                        <p>
                            As explained, software characteristics describe the requirements and the expectations of a software
                            in operational and technical levels. Thus, when a product owner says they are competing in a rapidly
                            changing markets, and they should adapt their business model quickly. The software should be
                            “extendable, modular and <strong>maintainable</strong>” if a business deals with urgent requests
                            that need to be
                            completed successfully in the matter of time. As a software architect, you should note that the
                            performance and low fault tolerance, <u>scalability and reliability</u> are your key
                            characteristics. Now,
                            after defining the previous characteristics the business owner tells you that they have a limited
                            budget for that project, another characteristic
                        </p>
                        <p>comes up here which is <b>“the feasibility.”</b></p>
                        <p>Here you can find a full list of software characteristics, also known as “quality attributes,”
                            <a href="#">here.</a>
                        </p>

                        <img class="img_md" src="<?= Yii::$app->homeUrl ?>web/img/inner/0654.png" alt="">
                        <p class="margin-b-3"><b>SOLID</b> refers to Single Responsibility, <mark>Open Closed</mark>, Liskov
                            substitution,
                            Interface
                            Segregation and
                            Dependency Inversion Principles.
                        </p>
                        <h3>Software Design</h3>
                        <p>While software architecture is responsible for the skeleton and the high-level
                            infrastructure of a
                            software, the software design is responsible for the code level design such as, what each module is
                            doing, the classes scope, and the functions purposes, etc.</p>
                        <ul>
                            <li>
                                <span class="c-dark">Single Responsibility Principle</span> means that each class has to have one
                                single purpose, a
                                responsibility and a reason to change.
                            </li>
                            <li>
                                <span class="c-dark">Open Closed Principle:</span>
                                a class should be open for extension, but closed for modification. In simple words, you should
                                be able to add more functionality to the class but do not edit current functions in a way that
                                breaks existing code that uses it
                            </li>
                        </ul>
                        <div class="cover_video">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/inner/16387156.png" alt="">
                            <div class="icon_played">
                                <button type="button" class="btn btn_video" data-toggle="modal" data-src="https://www.youtube.com/embed/VvHoHw5AWTk" data-target="#mdllVideo">
                                    <div class="scale rounded-circle b play_video">
                                        <i class="tio play"></i>
                                    </div>
                                </button>
                            </div>
                        </div>
                        <p class="margin-b-3"><b>SOLID</b> refers to Single Responsibility, <mark>Open Closed</mark>, Liskov
                            substitution,
                            Interface
                            Segregation and
                            Dependency Inversion Principles.
                        </p>
                        <p class="txt_quotation">
                            Thanks for reading! If you are interested in machine learning (or just want to understand what it
                            is), check out my Machine Learning is Fun! series too.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
<?php // } 
?>
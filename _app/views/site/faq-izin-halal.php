<?php

/** @var yii\web\View $this */

use yii\helpers\FileHelper;
use yii\helpers\Html;

$this->title = 'Izin Halal Kosmetik';

?>


<!-- Start banner_about -->
<section class="pt_banner_inner banner_px_image">
    <div class="parallax_cover">
        <img class="cover-parallax h-100vh" src="<?= Yii::$app->homeUrl ?>web/maklon/header.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8">
                <div class="banner_title_inner c-white">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        Izin Halal Kosmetik
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100" class="banner_title_inner c-yollow">
                        Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End banner_about -->

<section class="faq_one_inner py-0 mt-0 margin-b-6 w-100">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-5">
                <div class="title_sections_inner margin-b-5">

                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <!-- block Collapse -->
                <div class="faq_section faq_demo3 faq_with_icon">
                    <div class="block_faq">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header " id="headingOne">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Sertifikat Halal
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="mb-0">Sertifikat Halal MUI adalah fatwa tertulis Majelis Ulama Indonesia yang
                                            menyatakan kehalalan suatu produk sesuai dengan syari’at Islam. Sertifikat
                                            Halal MUI ini merupakan syarat untuk mendapatkan ijin pencantuman label
                                            halal pada kemasan produk dari instansi pemerintah yang berwenang.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Tujuan Sertifikasi Halal
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="mb-0">Sertifikasi Halal MUI pada produk pangan, obat-obat, kosmetika dan produk
                                            lainnya dilakukan untuk memberikan kepastian status kehalalan, sehingga
                                            dapat menenteramkan batin konsumen dalam mengkonsumsinya.
                                            Kesinambungan proses produksi halal dijamin oleh produsen dengan cara
                                            menerapkan Sistem Jaminan Halal.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
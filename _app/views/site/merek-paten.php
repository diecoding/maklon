<?php

/** @var yii\web\View $this */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Merek & Paten';

?>

<!-- Start banner_about -->
<section class="pt_banner_inner banner_px_image">
    <div class="parallax_cover">
        <img class="cover-parallax h-100vh" src="<?= Yii::$app->homeUrl ?>web/maklon/header.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-6">
                <div class="banner_title_inner c-white">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        Merek & Paten
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100" class="banner_title_inner c-yollow">
                        Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End banner_about -->

<!-- Start Services -->
<section class="services_section padding-t-5">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <div class="title_sections_inner margin-b-5">
                    <h3>Tidak hanya produksi, kami juga membantu
                        Anda mengurus perizinan BPOM, Hak Paten,
                        dan Sertifikat Halal. Bantuan ini mempercepat
                        produk Anda masuk ke Pasar.
                    </h3>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-8 mb-4 mb-lg-0 margin-b-5">
                <div class="items_serv">
                    <div class="media">
                        <div>
                            <img src="<?= Yii::$app->homeUrl ?>web/maklon/sertifikat-bpom.png" alt="" style="width: 80%;" />
                        </div>
                        <div class="media-body">
                            <h3>IZIN BPOM KOSMETIK</h3>
                            <p>Jangan biarkan kosmetik anda menjadi produk
                                ilegal atau tidak terdaftar secara resmi. Tim
                                kami akan membantu anda dalam setiap proses
                                dan prosedur yang terlibat dalam memperoleh
                                izin BPOM Produk anda.
                            </p>
                            <br>
                            <a href="<?= Url::toRoute(['/site/faq-izin-bpom']) ?>" class="btn btn_md_primary sweep_top sweep_letter bg-green2 c-white rounded-8">
                                <div class="inside_item">
                                    <span data-hover="Pelajari Lebih Lanjut">Pelajari Lebih Lanjut</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-8 mb-4 mb-lg-0 margin-b-5">
                <div class="items_serv">
                    <div class="media">
                        <div>
                            <img src="<?= Yii::$app->homeUrl ?>web/maklon/hki-250x200.png" alt="" style="width: 80%;" />
                        </div>
                        <div class="media-body">
                            <h3>Izin HKI atau Paten Kosmetik</h3>
                            <p>Jangan sampai Brand anda ditiru orang lain,
                                Nose Herbalindo akan membantu anda untuk
                                pendaftaran merk, pendaftaran hak paten,
                                pendaftaran hak cipta dan pendaftaran lainnya.
                            </p>
                            <br>
                            <a href="<?= Url::toRoute(['/site/faq-izin-hki']) ?>" class="btn btn_md_primary sweep_top sweep_letter bg-green2 c-white rounded-8">
                                <div class="inside_item">
                                    <span data-hover="Pelajari Lebih Lanjut">Pelajari Lebih Lanjut</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-8 mb-4 mb-lg-0 margin-b-5">
                <div class="items_serv">
                    <div class="media">
                        <div>
                            <img src="<?= Yii::$app->homeUrl ?>web/maklon/sertifikat-mui.png" alt="" style="width: 80%;" />
                        </div>
                        <div class="media-body">
                            <h3>IZIN HALAL KOSMETIK</h3>
                            <p>Adanya sertifikat halal MUI ini, menjadikan
                                tingkat kepercayaan yang tinggi bagi
                                masyarakat bahwa produk ini memang benar-benar halal, khususnya kosmetik yang
                                digunakan dalam aktivitas sehari-hari.
                            </p>
                            <br>
                            <a href="<?= Url::toRoute(['/site/faq-izin-halal']) ?>" class="btn btn_md_primary sweep_top sweep_letter bg-green2 c-white rounded-8">
                                <div class="inside_item">
                                    <span data-hover="Pelajari Lebih Lanjut">Pelajari Lebih Lanjut</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php

/** @var yii\web\View $this */
/** @var string $name */
/** @var string $message */
/** @var Exception$exception */

use yii\helpers\Html;

$this->title = $name;

?>

<div class="site-error m-5 text-center" style="min-height: 75vh; padding-top: 120px">

    <h1 class="mt-5 fw-bold"><?= Html::encode($this->title) ?></h1>

    <h4 class="my-4">
        <?= nl2br(Html::encode($message)) ?>
    </h4>

    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>

</div>
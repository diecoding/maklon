<?php

/** @var yii\web\View $this */

use yii\helpers\FileHelper;
use yii\helpers\Html;

$this->title = 'Izin BPOM Kosmetik';

?>


<!-- Start banner_about -->
<section class="pt_banner_inner banner_px_image">
    <div class="parallax_cover">
        <img class="cover-parallax h-100vh" src="<?= Yii::$app->homeUrl ?>web/maklon/header.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8">
                <div class="banner_title_inner c-white">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        Izin BPOM Kosmetik
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100" class="banner_title_inner c-yollow">
                        Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End banner_about -->

<section class="faq_one_inner py-0 mt-0 margin-b-6 w-100">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-5">
                <div class="title_sections_inner margin-b-5">

                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <!-- block Collapse -->
                <div class="faq_section faq_demo3 faq_with_icon">
                    <div class="block_faq">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header " id="headingOne">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Apa Itu BPOM Kosmetik?
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            Badan Pengawas Obat dan Makanan atau disingkat BPOM merupakan sebuah lembaga yang
                                            bertugas mengawasi peredaran obat-obatan dan makanan di Indonesia. Sistem Pengawasan Obat
                                            dan dan Makanan (SisPOM) ini efektif dan efisien mampu mendeteksi, mencegah dan mengawasi
                                            produk-produk di pasaran sehingga bisa melindungi keamanan, keselamatan dan kesehatan
                                            konsumennya baik yang berada di dalam maupun luar negeri.
                                        </p>
                                        <p>BPOM sendiri telah memiliki jaringan nasional dan internasional yang memiliki kewenangan dalam
                                            penegakan hukum dan memiliki kredibilitas profesional yang tinggi. Tugas Utama BPOM
                                            Berdasarkan Pasal 67 Keputusan Presiden Nomor 103 Tahun 2001 adalah melaksanakan tugas
                                            pemerintahan di bidang pengawasan Obat dan Makanan sesuai dengan ketentuan peraturan
                                            perundang-undangan yang berlaku.
                                        </p>
                                        <p class="mb-0">Sedangkan Tugas Balai Besar/Balai POM (Unit Pelaksana Teknis) Berdasarkan Pasal 2 Peraturan
                                            Kepala BPOM Nomor 14 Tahun 2014, Unit Pelaksana Teknis di lingkungan BPOM mempunyai tugas
                                            melaksanakan kebijakan di bidang pengawasan obat dan makanan, yang meliputi pengawasan atas
                                            produk terapetik, narkotika, psikotropika, zat adiktif, obat tradisional, kosmetik, produk
                                            komplemen serta pengawasan atas keamanan pangan dan bahan berbahaya.</p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Apa Fungsi Bpom Kosmetik?
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Berdasarakan Pasal 68 Keputusan Presiden Nomor 103 Tahun 2001, BPOM Mempunyai Fungsi:</p>
                                        <div class="features_points">
                                            <ul class="list-group list_feat">
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="0">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pengkajian dan penyusunan kebijakan nasional di bidang pengawasan Obat dan Makanan.</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="100">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pelaksanaan kebijakan tertentu di bidang pengawasan Obat dan Makanan.</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Koordinasi kegiatan fungsional dalam pelaksanaan tugas BPOM.</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pemantauan, pemberian bimbingan dan pembinaan terhadap kegiatan instansi pemerintah di bidang pengawasan Obat dan Makanan.</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Penyelenggaraan pembinaan dan pelayanan administrasi umum di bidang perencanaan umum, ketata-usahaan, organisasi dan tata laksana, kepegawaian, keuangan, kearsipan, persandian, perlengkapan dan rumah tangga.</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <p>Sementara Fungsi Balai Besar/Balai POM (Unit Pelaksana Teknis):
                                        </p>
                                        <p>Berdasarkan Pasal 3 Peraturan Kepala BPOM Nomor 14 Tahun 2014, Unit Pelaksana Teknis di lingkungan BPOM mempunyai fungsi:</p>
                                        <div class="features_points">
                                            <ul class="list-group list_feat">
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="0">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pengkajian dan penyusunan kebijakan nasional di bidang pengawasan Obat dan Makanan.</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="100">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Penyusunan rencana dan program pengawasan obat dan makanan.</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pelaksanaan pemeriksaan secara laboratorium, pengujian dan penilaian mutu produk terapetik, narkotika, psikotropika zat adiktif, obat tradisional, kosmetik, produk komplemen, pangan dan bahan berbahaya.</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pelaksanaan pemeriksaan laboratorium, pengujian dan penilaian mutu produk secara mikrobiologi.</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pelaksanaan pemeriksaan setempat, pengambilan contoh dan pemeriksaan sarana produksi dan distribusi</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Investigasi dan penyidikan pada kasus pelanggaran hukum</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pelaksanaan sertifikasi produk, sarana produksi dan distribusi tertentu yang ditetapkan oleh Kepala Badan Pengawas Obat dan Makanan</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pelaksanaan kegiatan layanan informasi konsumen</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Evaluasi dan penyusunan laporan pengujian obat dan makanan</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pelaksanaan urusan tata usaha dan kerumah-tanggaan</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pelaksanaan tugas lain yang ditetapkan oleh Kepala Badan Pengawas Obat dan Makanan, sesuai dengan bidang tugasnya</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
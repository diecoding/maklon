<?php

/** @var yii\web\View $this */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'ODM - Maklon Nutrisi & Beauty';

?>

<!-- Start banner_about -->
<section class="pt_banner_inner banner_px_image">
    <div class="parallax_cover">
        <img class="cover-parallax h-100vh" src="<?= Yii::$app->homeUrl ?>web/maklon/header.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-12">
                <div class="banner_title_inner c-white">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        ODM - Original Design Manufacture
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100" class="banner_title_inner c-yollow">
                        Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End banner_about -->

<section class="team_overlay_style team_default_style padding-t-10">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-8">
                <div class="item_group">
                    <div class="image_ps">
                        <img src="<?= Yii::$app->homeUrl ?>web/maklon/OEM.jpg" alt="">
                        <div class="content_txt left-side">
                            <h3>ODM</h3>
                            <p>Kami membantu anda membangun produk
                                nutrisi & beauty anda. Mulai dari konsep
                                produk, formula, R&D, produksi hingga
                                distribusinya (toko online).
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6 col-lg-12">
                <div class="media-body">
                    <h3>Jasa Pembuatan Kosmetik ODM</h3>
                    <p>Merupakan sistem layanan total bagi kami yang bertanggung
                        jawab atas semua proses produksi mulai dari perencanaan
                        produk, pengembangan, produksi hingga manajemen kualitas
                        dan pengiriman berdasarkan teknologi, pengalaman dan
                        pengetahuan kami.</p>
                    <p>Jika anda ingin menciptakan desain dan spesifikasi produk
                        kosmetik yang anda inginkan secara keseluruhan dan memiliki
                        sumber daya yang terbatas dalam membangun dan membuat
                        desain, maka perusahaan ODM dapat membuat konsep yang
                        anda inginkan menjadi barang yang nyata dan laku dipasaran.</p>
                    <p>Apapun yang anda butuhkan dan harapkan untuk perusahaan
                        kosmetik anda, kami akan siap menangani R&D, konsep produk,
                        pengujian dan memanufakturkannya
                    </p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center text-center">
            <div class="col-md-8 col-lg-12">
                <div class="title_sections">
                    <a href="<?= Url::toRoute(['/site/kontak-kami']) ?>" class="btn btn_md_primary margin-t-2 bg-green2 c-white sweep_top sweep_letter rounded-12">
                        <div class="inside_item">
                            <span data-hover="Buat Produk OEM">Buat Produk OEM</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-lg-12">
                <div class="title_sections">
                    <h3>Free Jasa pembuatan Toko Online dengan sharing profit penjualan</h3>
                    <ul style="color: #6c7a87;">
                        <li>Hingga 100% Pembandingan yang Akurat</li>
                        <li>Memenuhi Kebutuhan dan Anggaran Anda</li>
                        <li>Stabilitas & Kompatibilitas Diuji</li>
                    </ul>
                </div>
            </div>

            <div class="col-md-8 col-lg-12">
                <div class="title_sections">
                    <!-- <div class="before_title">
                        <span class="c-green2">Integration</span>
                    </div> -->
                    <h3>Jasa pembuatan Toko Online tanpa sharing profit penjualan (beli putus)</h3>
                </div>
            </div>
        </div>
    </div>
</section>
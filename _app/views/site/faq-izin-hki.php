<?php

/** @var yii\web\View $this */

use yii\helpers\FileHelper;
use yii\helpers\Html;

$this->title = 'Izin HKI Kosmetik';

?>


<!-- Start banner_about -->
<section class="pt_banner_inner banner_px_image">
    <div class="parallax_cover">
        <img class="cover-parallax h-100vh" src="<?= Yii::$app->homeUrl ?>web/maklon/header.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8">
                <div class="banner_title_inner c-white">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        Izin HKI Kosmetik
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100" class="banner_title_inner c-yollow">
                        Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End banner_about -->

<section class="faq_one_inner py-0 mt-0 margin-b-6 w-100">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-5">
                <div class="title_sections_inner margin-b-5">

                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <!-- block Collapse -->
                <div class="faq_section faq_demo3 faq_with_icon">
                    <div class="block_faq">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header " id="headingOne">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Apa Itu Merek?
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Merek bisa jadi merupakan bentuk perlindungan HKI yang paling dekat dengan kehidupan kita sehari-hari. Barang atau jasa
                                            apapun yang kita butuhkan, lebih sering kita sebut dengan nama dagangnya ketimbang nama generiknya. Sejak sebelum memulai
                                            aktivitas pagi hari, Anda sarapan Sari Roti ditemani secangkir Nescafe Classic sambil membaca Kompas Online di iPad, baru
                                            pergi naik Innova menuju kantor, sudah berapa merek yang Anda sebutkan?
                                        </p>
                                        <p>
                                            <b>Merek</b> – atau juga biasa dikenal dengan istilah brand – adalah penanda identitas dari sebuah produk barang atau jasa yang ada
                                            dalam perdagangan. Namun tidak hanya sebagai identitas semata, merek juga berperan penting mewakili reputasi tidak hanya
                                            produknya, namun juga penghasil dari produk barang/jasa yang dimaksud. Tak heran jika branding menjadi bagian yang sangat
                                            penting dalam pemasaran suatu produk/jasa.
                                        </p>
                                        <p>
                                            <b>Hak Merek</b> adalah bentuk perlindungan HKI yang memberikan hak eksklusif bagi pemilik merek terdaftar untuk menggunakan
                                            merek tersebut dalam perdagangan barang dan/atau jasa, sesuai dengan kelas dan jenis barang/jasa untuk mana merek
                                            tersebut terdaftar.
                                        </p>
                                        <p class="mb-0">Satu hal yang perlu dipahami adalah, pendaftaran Merek untuk memperoleh Hak Merek bukan berarti ijin untuk menggunakan
                                            merek itu sendiri. Siapapun berhak memakai merek apapun – didaftar ataupun tidak - sepanjang tidak sama dengan merek
                                            terdaftar milik orang lain di kelas dan jenis barang/jasa yang sama. Hanya saja, dengan merek terdaftar, si pemilik merek punya
                                            hak melarang siapapun untuk menggunakan merek yang sama dengan merek terdaftar miliknya tadi, tentunya untuk kelas dan
                                            jenis barang/jasa yang sama.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Merek Seperti Apa yang Dapat Diberi Perlindungan Sebagai Merek Terdaftar?
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Suatu merek yang dapat didaftar harus memiliki daya pembeda dan dipergunakan dalam perdagangan barang/jasa, dan dapat berupa:</p>
                                        <div class="features_points">
                                            <ul class="list-group list_feat">
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="0">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>gambar, seperti lukisan burung garuda pada logo Garuda Indonesia atau gambar kelinci pada logo Dua Kelinci;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="100">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>kata, seperti Google, Toyota, atau Mandiri;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>nama, seperti Tommy Hilfiger atau Salvatore Ferragamo;</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>frasa, seperti Sinar Jaya atau Air Mancur;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>kalimat, seperti Building for a Better Future atau Terus Terang Philip Terang Terus;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>huruf, seperti huruf “F” pada logo Facebook atau huruf “K” pada logo Circle-K;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>huruf-huruf, seperti IBM atau DKNY;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>angka, seperti angka “7” pada logo Seven Eleven atau angka “3” pada logo provider GSM Three;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>angka-angka, seperti merek rokok 555 atau merek wewangian 4711;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>susunan warna, seperti pada logo Pepsi atau Pertamina;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>bentuk 3 (tiga) dimensi;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>suara;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>hologram;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>kombinasi dari unsur-unsur tersebut.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <p>Suatu Merek tidak dapat didaftar apabila:</p>
                                        <div class="features_points">
                                            <ul class="list-group list_feat">
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="0">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Pendaftarannya dilandasi dengan itikad buruk. Katakanlah seorang pengusaha ayam goreng mendaftarkan merek CIPUTAT FRIED CHICKEN di kelas dan jenis barang-barang hasil olahan daging ayam. Jika ada pengusaha lain yang mencoba mendaftarkan merek yang sama untuk kelas dan jenis jasa restoran dengan niatan untuk menghalangi pengusaha pertama, maka pendaftaran ke dua bisa dianggap dengan itikad tidak baik dan dengan demikian semestinya tidak dapat didaftar;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="100">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Contoh Buddha Bar yang kemudian dibatalkan karena dianggap bertentangan dengan agama;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Tidak memiliki daya pembeda, misalnya tanda tanya “?” atau huruf balok tunggal “K” dalam perwujudan yang biasa/lazim. Namun tanda tanya “?” yang diberi ornamen seperti pada logo Guess, atau huruf tunggal “K” yang ditampilkan dalam tata artistik tertentu seperti pada logo Circle-K, bisa didaftar;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>telah menjadi milik umum, seperti tanda tengkorak bajak laut atau palang seperti pada palang merah. Namun jika diberi ornamen tambahan seperti tengkorak pada logo Skullcandy atau palang pada logo Swiss Army, bisa didaftar;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Menerangkan barang/jasanya itu sendiri. Apple tidak dapat didaftarkan sebagai merek untuk buah-buahan, tapi bisa didaftar untuk merek produk elektronik.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>

                                        <p>Selain itu pendaftaran suatu merek juga harus ditolak oleh DJHKI jika merek yang akan didaftar mempunyai persamaan baik keseluruhan maupun pada pokoknya dengan:
                                        </p>
                                        <div class="features_points">
                                            <ul class="list-group list_feat">
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="0">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Merek terdaftar milik pihak lain untuk barang/jasa yang sejenis. Ketika A sudah memiliki merek terdaftar GEULIS untuk jenis barang pakaian jadi, pendaftaran GEULIS, GEULEES, atau GAULIES oleh B pada jenis barang pakaian jadi akan ditolak;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="100">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Merek terkenal milik pihak lain. Kriteria baku merek terkenal sebenarnya belum diatur secara resmi dalam Peraturan Pemerintah. Biasanya penentuan apakah suatu merek dapat dianggap terkenal atau tidak dilihat dari adanya pendaftaran di sejumlah negara; atau
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Indikasi geografis yang sudah dikenal. Kintamani misalnya, tidak dapat didaftar sebagai merek untuk kopi, karena sudah ada indikasi geografis Kopi Kintamani. Demikian pula Parmigiana Reggiano untuk keju dan olahan susu, atau Champagne untuk minuman beralkohol.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <p>Di samping itu pendaftaran juga harus ditolak jika merek:
                                        </p>
                                        <div class="features_points">
                                            <ul class="list-group list_feat">
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="0">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Merupakan atau menyerupai nama orang terkenal, foto, atau nama badan hukum milik orang lain kecuali sudah ada persetujuan;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="100">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Merupakan tiruan atau menyerupai nama atau singkatan nama, bendera, lambang, simbol, atau emblem negara, lembaga nasional, atau lembaga internasional kecuali sudah ada persetujuan; atau
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Merupakan tiruan atau menyerupai tanda, cap atau stempel resmi yang digunakan negara atau lembaga pemerintah, kecuali sudah ada persetujuan tertulis.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header " id="headingOne">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                            Siapa yang Berhak Mendaftarkan Merek?
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseThree" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Satu konsep yang harus dipahami dalam sistem perlindungan merek – khususnya yang berlaku di Indonesia – adalah bahwa sejatinya istilah yang tepat bukanlah “pemilik merek”, melainkan “pemilik/pemegang hak atas merek terdaftar”, karena sang pemilik hak tersebut memperoleh haknya melalui klaimnya dalam bentuk pendaftaran ke DJHKI.
                                        </p>
                                        <p>Suatu merek bebas dipergunakan – bukan dimiliki – oleh siapa saja, sampai ada orang yang mengklaim hak eksklusif atas merek tersebut melalui pendaftaran.
                                        </p>
                                        <p class="mb-0">Prinsip first to file yang dianut dalam sistem perlindungan Merek di Indonesia membuat siapapun – baik perorangan maupun badan hukum – yang pertama kali mendaftarkan suatu merek untuk kelas dan jenis barang/jasa tertentu, dianggap sebagai pemilik hak atas merek yang bersangkutan untuk kelas dan jenis barang/jasa tersebut.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header " id="headingOne">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                            Kapan Sebaiknya Suatu Merek Didaftar?
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseFour" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Tidak seperti Paten atau Hak Cipta, perlindungan Merek Terdaftar tidak mempersyaratkan baik “kebaruan (novelty)” ataupun “keaslian (
                                            originality)”.
                                        </p>
                                        <p>Dengan demikian suatu merek yang sudah dipergunakan secara luas selama bertahun-tahun tetap masih bisa didaftar, sepanjang memang tidak
                                            memiliki persamaan baik secara keseluruhan maupun pada pokoknya dengan merek milik pihak lain yang telah lebih dahulu didaftar atau
                                            diajukan permohonan pendaftarannya.
                                        </p>
                                        <p class="mb-0">Hal ini tidak berarti pendaftaran merek tidak time-sensitive sama sekali. Merek juga menganut prinsip first to file, sehingga kelalaian
                                            seseorang untuk mendaftarkan suatu merek untuk barang/jasa yang ia perdagangkan bisa berakibat ia keduluan oleh orang lain mendaftarkan
                                            merek yang sama/mirip untuk barang/jasa sejenis, sehingga ia bisa kehilangan hak untuk mempergunakan mereknya sendiri yang sudah ia
                                            pergunakan lebih dahulu.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header " id="headingOne">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                                            Dimanakah Sajakah Perlindungan Merek Berlaku?
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseFive" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Merek menganut prinsip teritorial, yang artinya perlindungan merek hanya berlaku di negara di mana permohonan paten diajukan dan diberi.
                                            Untuk memperoleh perlindungan merek di wilayah hukum Indonesia, maka sang inventor harus mengajukan permohonan merek di Indonesia,
                                            dalam hal ini ke Direktorat Jenderal Hak Kekayaan Intelektual (DJHKI). Di sisi lain merek yang hanya didaftar di Indonesia, tidak memiliki
                                            perlindungan di negara lain.
                                        </p>
                                        <p>Untuk mendaftarkan merek di luar negeri, pemohon harus mendaftarkan merek tersebut sendiri-sendiri di masing-masing negara yang
                                            dikehendaki, dengan menunjuk Konsultan HKI Terdaftar yang wilayah kerjanya meliputi negara tersebut, untuk menjadi Kuasa permohonan.
                                        </p>
                                        <p class="mb-0">Dalam kurun waktu 6 bulan sejak Tanggal Penerimaan pertama kali di Indonesia, pemohon bisa mengajukan permohonan pendaftaran untuk
                                            merek yang sama untuk barang/jasa sejenis di negara lain yang sama-sama menjadi anggota Konvensi Paris dan mendapatkan Tanggal
                                            Penerimaan yang sama dengan Tanggal Penerimaan di Indonesia dengan menggunakan Hak Prioritas.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header " id="headingOne">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                            Bagaimana Tatacara dan Prosedur Untuk Mendaftarkan Merek?
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseSix" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Merek menganut prinsip teritorial, yang artinya perlindungan merek hanya berlaku di negara di mana permohonan paten diajukan dan diberi.
                                            Untuk memperoleh perlindungan merek di wilayah hukum Indonesia, maka sang inventor harus mengajukan permohonan merek di Indonesia,
                                            dalam hal ini ke Direktorat Jenderal Hak Kekayaan Intelektual (DJHKI). Di sisi lain merek yang hanya didaftar di Indonesia, tidak memiliki
                                            perlindungan di negara lain.</p>
                                        <p>Untuk mendaftarkan merek di luar negeri, pemohon harus mendaftarkan merek tersebut sendiri-sendiri di masing-masing negara yang
                                            dikehendaki, dengan menunjuk Konsultan HKI Terdaftar yang wilayah kerjanya meliputi negara tersebut, untuk menjadi Kuasa permohonan.
                                        </p>
                                        <p>Dalam kurun waktu 6 bulan sejak Tanggal Penerimaan pertama kali di Indonesia, pemohon bisa mengajukan permohonan pendaftaran untuk
                                            merek yang sama untuk barang/jasa sejenis di negara lain yang sama-sama menjadi anggota Konvensi Paris dan mendapatkan Tanggal
                                            Penerimaan yang sama dengan Tanggal Penerimaan di Indonesia dengan menggunakan Hak Prioritas.</p>
                                        <p>Dokumen dan persyaratan yang harus dilengkapi saat pengajuan untuk mendapatkan Tanggal Penerimaan adalah:</p>
                                        <div class="features_points">
                                            <ul class="list-group list_feat">
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="0">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Formulir Pendaftaran Merek yang dibuat rangkap dua, telah diisi lengkap dan ditanda-tangani oleh Pemohon atau Kuasanya;</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="100">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Kelas dan jenis barang/jasa. Satu permohonan merek untuk satu merek di satu kelas, namun tidak terbatas jumlah jenis barang/jasanya. Kelas
                                                        dan jenis barang tidak dapat diganti ataupun ditambah setelah mendapat Tanggal Penerimaan, namun untuk jenis barang dapat dikurangi</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Membayar biaya pendaftaran (biaya ini adalah biaya dasar per merek per kelas yang berlaku jika pemohon mengajukan secara langsung ke loket
                                                        DJKI atau melalui Kanwil Kemenkum HAM. Untuk pendaftaran online via Konsultan HKI Terdaftar akan menimbulkan biaya jasa yang
                                                        besarannya bergantung pada masing-masing Konsultan HKI sebagai penyedia jasa).;</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Contoh etiket merek sebanyak 3 (tiga) lembar, dengan ukuran minimum 2 x 2 cm dan maksimum 9 x 9 cm;</p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Surat Pernyataan Hak, yang merupakan pernyataan Pemohon bahwa ia memang memiliki hak untuk mengajukan pendaftaran merek tersebut
                                                        dan akan menggunakan merek yang didaftarkan dalam perdagangan barang/jasa untuk mana merek tersebut didaftar;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Surat Kuasa, jika permohonan diajukan melalui Kuasa.</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header " id="headingOne">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                                            Waktu & Biaya
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseSeven" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Merek menganut prinsip teritorial, yang artinya perlindungan merek hanya berlaku di negara di mana permohonan paten diajukan dan diberi. Dari uraian
                                            sebelumnya, proses permohonan pendaftaran merek dari sejak Tanggal Penerimaan hingga Tanggal Pendaftaran memakan waktu sekitar 7 hingga 9 bulan. Hal ini
                                            merupakan terobosan yang diatur dalam UU merek yang baru, UU no. 20 tahun 2016 tentang Merek dan Indikasi Geografis.
                                        </p>
                                        <p>Sebelumnya berdasarkan UU no. 15 tahun 2001 tentang Merek, jangka waktu pemroresan permohonan adalah sekitar 12 hingga 18 bulan. Namun pada
                                            prakteknya DJKI kesulitan memenuhi jangka waktu tersebut, terutama disebabkan oleh tingginya volume permohonan yang masuk berbanding dengan tenaga
                                            pemeriksa yang dimiliki oleh DJKI. Secara umum, biasanya satu permohonan saat ini akan memakan waktu antara 18-24 bulan sampai terbitnya Sertifikat.
                                        </p>
                                        <p>Pemohon tidak dapat mengambil tindakan hukum apapun terhadap pihak lain yang menggunakan merek tanpa ijin selama Sertifikat Merek belum terbit, namun
                                            setelah merek tersebut didaftar Pemegang Hak Merek dapat menuntut ganti kerugian atas pelanggaran merek yang dilakukan setelah Tanggal Penerimaan.
                                        </p>
                                        <p class="mb-0">Komponen Biaya Permohonan Merek setiap satu merek di satu kelas, tanpa ada batasan untuk jumlah jenis barang atau jasa yang dicantumkan sepanjang
                                            masih dalam kelas yang sama. Tentunya komponen biaya ini belum termasuk biaya jasa profesional apabila permohonan diajukan melalui Konsultan HKI
                                            Terdaftar.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header " id="headingOne">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                                            Perpanjangan Merek
                                        </button>
                                    </h3>
                                </div>
                                <div id="collapseEight" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Masa perlindungan Hak Merek berlaku selama 10 tahun sejak Tanggal Penerimaan. Jika Tanggal Penerimaan permohonan pendaftaran suatu merek adalah
                                            1 Oktober 2017, maka perlindungannya akan berlaku hingga 1 Oktober 2027.
                                        </p>
                                        <p>Masa perlindungan Hak Merek dapat diperpanjang setiap 10 tahun secara terus menerus.
                                        </p>
                                        <p>Pemegang Hak Merek sudah dapat mengajukan permohonan perpanjangan merek dari sejak enam bulan sebelum berakhirnya masa perlindungan merek
                                            sampai dengan 6 bulan sesudah masa perlindungan berakhir.
                                        </p>
                                        <p>Dalam contoh di atas, pemegang hak merek sudah dapat mengajukan permohonan perpanjangan sejak 1 April 2027 hingga 1 April 2028.
                                        </p>
                                        <p>Syarat mengajukan permohonan perpanjangan merek adalah:</p>
                                        <div class="features_points">
                                            <ul class="list-group list_feat">
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="0">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Mengisi formulir permohonan perpanjangan merek yang dibuat rangkap empat, diisi lengkap dan ditanda-tangani oleh pemohon atau kuasanya;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="100">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Membayar biaya perpanjangan jika permohonan diajukan sebelum berakhirnya masa perlindungan, jika permohonan diajukan sesudah berakhirnya masa
                                                        perlindungan;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Fotokopi Sertiifikat Merek yang akan diperpanjang;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Surat Pernyataan Hak, yang merupakan pernyataan Pemohon bahwa ia memang memiliki hak untuk mengajukan perpanjangan merek tersebut dan tetap
                                                        akan menggunakan merek yang diperpanjang dalam perdagangan barang/jasa untuk mana merek tersebut didaftar;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Surat Kuasa, jika permohonan diajukan melalui Kuasa;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Fotokopi KTP/Identitas Pemohon, jika Pemohon perorangan;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Fotokopi Akta Pendirian Badan Hukum yang telah dilegalisir, jika Pemohon adalah Badan Hukum;
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Fotokopi NPWP Badan Hukum, jika Pemohon adalah Badan Hukum; dan
                                                    </p>
                                                </li>
                                                <li class="list-group-item border-0" style="margin-bottom: 1rem;" data-aos="fade-up" data-aos-delay="200">
                                                    <i class="tio checkmark_circle_outlined"></i>
                                                    <p>Fotokopi KTP/Identitas orang yang bertindak atas nama Pemohon Badan Hukum untuk menandatangani Surat Pernyataan dan Surat Kuasa.
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
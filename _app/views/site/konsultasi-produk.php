<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'Konsultasi Produk';

?>

<!-- Start banner_about -->
<section class="pt_banner_inner banner_px_image">
    <div class="parallax_cover">
        <img class="cover-parallax h-100vh" src="<?= Yii::$app->homeUrl ?>web/maklon/header.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-12">
                <div class="banner_title_inner c-white">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        Konsultasi Produk
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100" class="banner_title_inner c-yollow">
                        Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End banner_about -->

<section class="about_cc_grid padding-t-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="title_sections_inner mb-0">
                    <div class="before_title">
                        <h2 class="c-orange-red">Ide dan Konsep Produk</h2>
                    </div>
                    <p>Apakah Anda ingin memiliki produk perawatan kecantikan Anda
                        sendiri, tetapi Anda tidak punya ide untuk membuat produk
                        jadi yang rapi, dengan kemasan menarik untuk target
                        pelanggan Anda?</p>
                </div>
            </div>
            <div class="col-lg-6 ml-auto">
                <div class="title_sections_inner mb-0">
                    <h5>Tim manajemen Proyek kami akan mendukung Anda dengan
                        pengalaman praktis kami dan menunjukkan kepada Anda
                        beberapa produk referensi untuk informasi Anda. Kami juga
                        dapat membantu Anda atau tim referensi Anda dalam
                        menghasilkan ide dan konsep baru, termasuk mengembangkan
                        merek Anda atau rangkaian produk yang sudah ada.
                    </h5>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="faq_one_inner my-0 w-100 pb-0" style="padding-top: 4rem;">
    <div class="container">
        <div class="features_points">
            <div class="title_sections_inner">
                <h5>Ide dan konsep produk berasal dari 'manfaat produk' yang
                    diinginkan pelanggan. Ide dan konsep produk dapat menjadi
                    seperti di bawah ini :</h5>
            </div>
            <ul class="list-group list_feat">
                <li class="list-group-item border-0" data-aos="fade-up" data-aos-delay="0">
                    <i class="tio checkmark_circle_outlined"></i>
                    <p>
                        Manfaat produk dasar - karakteristik fisik (seperti
                        ukuran, bentuk, warna), komposisi kimia, kualitas
                        sensorik, nilai bahan aktif, dan fitur keselamatan.
                    </p>
                </li>
                <li class="list-group-item border-0" data-aos="fade-up" data-aos-delay="100">
                    <i class="tio checkmark_circle_outlined"></i>
                    <p>
                        Paket manfaat termasuk harga, nilai uang,
                        kemudahan penyimpanan, penggunaan dan
                        pembuangan.
                    </p>
                </li>
                <li class="list-group-item border-0" data-aos="fade-up" data-aos-delay="200">
                    <i class="tio checkmark_circle_outlined"></i>
                    <p>
                        Manfaat psikologis termasuk premium,
                        normal, estetika dan kesehatan.
                    </p>
                </li>
            </ul>
        </div>
    </div>
</section>

<section class="content-Sblog py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="body_content">
                    <img class="img_md" src="<?= Yii::$app->homeUrl ?>web/maklon/OEM.jpg" alt="">
                    <p class="mt-4">Kami memiliki berbagai formulasi produk perawatan
                        kulit standar yang diformulasikan di bawah panduan
                        ISO 9001:2015 untuk memenuhi kriteria dan
                        persyaratan Anda untuk produk yang Anda minati.
                        <br>
                        Kami menggunakan bahan aktif alami untuk
                        meningkatkan efektivitasnya. Formulasi perawatan
                        kulit standar kami melalui produk pengujian inhouse seperti uji stabilitas dan uji kompatibilitas.
                    </p>
                </div>
                <div class="body_content pt-5">
                    <img class="img_md" src="<?= Yii::$app->homeUrl ?>web/maklon/OEM.jpg" alt="">
                    <p class="mt-4">Pemilihan kemasan produk adalah salah satu elemen penting dan berpengaruh untuk
                        menghadirkan produk Anda kepada pelanggan sasaran. Selain penampilan dan daya tarik
                        produk Anda, opsi pengemasan juga memberikan fungsionalitas, stabilitas produk, dan harga
                        yang kompetitif.
                        <br>
                        Tim R&D dapat membantu Anda memilih bahan kemasan yang sesuai, jenis kemasan, dan
                        alternatif yang sesuai dengan kategori produk Anda. Untuk desain logo dan kemasan, Anda
                        dapat menggunakan jasa outsource yang bisa membantu kembangkan dan menuangkan ide
                        Anda ke dalam desain yang beda dari yang lain.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
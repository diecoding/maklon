<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'Sertifikasi';

?>

<!-- Start banner_about -->
<section class="pt_banner_inner banner_px_image">
    <div class="parallax_cover">
        <img class="cover-parallax h-100vh" src="<?= Yii::$app->homeUrl ?>web/maklon/header.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-6">
                <div class="banner_title_inner c-white">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        Sertifikasi
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100" class="banner_title_inner c-yollow">
                        Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End banner_about -->

<!-- Start Services -->
<section class="services_section padding-t-10">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-8 mb-4 mb-lg-0 margin-b-5">
                <div class="items_serv">
                    <div class="media">
                        <div>
                            <img src="<?= Yii::$app->homeUrl ?>web/maklon/sertifikat-gmp.png" alt="" style="width: 80%;"/>
                        </div>
                        <div class="media-body">
                            <h3>GMP</h3>
                            <p>Sistem produksi di perusahaan ini sudah tersertifikasi
                                dengan standar GMP atau yang disebut juga Good
                                Manufacturing Practice, sertifikasi ini menunjukkan
                                keamanan produksi yang pastinya telah terjaga dan ter
                                kontrol, sehingga kemanan produksi kosmetik anda dari
                                tahap formulasi hingga packaging serta pendistribusian
                                nya bisa terjamin keamanan dan konsistensinya.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-8 mb-4 mb-lg-0 margin-b-5">
                <div class="items_serv">
                    <div class="media">
                        <div>
                            <img src="<?= Yii::$app->homeUrl ?>web/maklon/sertifikat-ohsas.png" alt="" style="width: 80%;" />
                        </div>
                        <div class="media-body">
                            <h3>OHSAS</h3>
                            <p>OHSAS adalah sistem manajemen k3 standar internasional
                                untuk keselamatan kerja dan kesehatan, kepanjangan dari
                                OHSAS adalah Occupational Health and Safety Assesment
                                Series, jadi dengan adanya sertifikasi ini, kepastian
                                produksi bisa lebih terjaga dan keselamatan dalam
                                produksi bisa lebih maksimal, sehingga anda tidak perlu
                                khawatir untuk memproduksi kosmetik di perusahaan kami.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-8 mb-4 mb-lg-0 margin-b-5">
                <div class="items_serv">
                    <div class="media">
                        <div>
                            <img src="<?= Yii::$app->homeUrl ?>web/maklon/sertifikat-bpom.png" alt="" style="width: 80%;" />
                        </div>
                        <div class="media-body">
                            <h3>BPOM</h3>
                            <p>Semua produk kosmetik yang kami hasilkan pastinya
                                terdaftar dan tersertifikasi di BPOM, seperti yang kita
                                ketahui BPOM adalah lembaga yang mengawasi peredaran
                                produk makanan dan obat-obatan, termasuk kosmetik,
                                BPOM adalah singkatan dari Badan Pengawas Obat dan
                                Makanan, Jadi anda tidak perlu ragu, jika produk anda
                                sudah disetujui dan mendapat sertifikasi dari BPOM maka
                                produk anda sudah siap dijual di pasaran.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-8 mb-4 mb-lg-0 margin-b-5">
                <div class="items_serv">
                    <div class="media">
                        <div>
                            <img src="<?= Yii::$app->homeUrl ?>web/maklon/sertifikat-gmp.png" alt="" style="width: 80%;" />
                        </div>
                        <div class="media-body">
                            <h3>HACCP</h3>
                            <p>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-8 mb-4 mb-lg-0 margin-b-5">
                <div class="items_serv">
                    <div class="media">
                        <div>
                            <img src="<?= Yii::$app->homeUrl ?>web/maklon/sertifikat-mui.png" alt="" style="width: 80%;" />
                        </div>
                        <div class="media-body">
                            <h3>HALAL</h3>
                            <p>Sertifikasi Halal ini dikeluarkan oleh LPPOM MUI,
                                merupakan singkatan dari Lembaga Pengkajian Pangan,
                                Obat-Obatan, dan Kosmetika Majelis Ulama Indonesia,
                                sertifikasi ini menjamin ke-halalan produk mulai dari
                                formulasi, bahan baku, produksi, pengemasan, hingga
                                sampai di tangan konsumen, dengan adanya sertifikasi ini
                                membuat produk yang kami hasilkan sangat aman dan
                                halal untuk digunakan.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
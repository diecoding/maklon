<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'Research & Development';

?>

<!-- Start banner_about -->
<section class="pt_banner_inner banner_px_image">
    <div class="parallax_cover">
        <img class="cover-parallax h-100vh" src="<?= Yii::$app->homeUrl ?>web/maklon/header.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-12">
                <div class="banner_title_inner c-white">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        Research & Development
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100" class="banner_title_inner c-yollow">
                        Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End banner_about -->

<!-- Start Services -->

<section class="service__about padding-py-5">
    <div class="container">
        <div class="body__tab">
            <div class="row justify-content-center text-center">
                <div class="col-lg-12">
                    <div class="title_sections_inner margin-b-5">
                        <h3>Kami dapat menemukan solusi formulasi yang
                            paling inovatif, segar, dan bernilai baru untuk
                            masalah perawatan kulit.
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="title_sections_inner margin-b-6" data-aos="fade-up" data-aos-delay="0">
                        <p>Kami memiliki ahli kosmetik dan ilmuwan yang
                            mengidentifikasi dan menggunakan teknologi
                            dan metode baru untuk menghasilkan dan
                            memberikan produk kualitas terbaik kepada
                            pelanggan dan konsumen kami.
                        </p>
                        <p>Inovasi yang sesuai dengan kebutuhan hidup
                            manusia merupakan kewajiban yang kami
                            penuhi bagi pelanggan berharga kami. Tim R&D
                            kami memiliki pengetahuan yang komprehensif
                            di bidang kosmetik dan perawatan kulit.
                        </p>
                        <p>Berikut adalah layanan kami yang Anda
                            dapatkan jika maklon dengan kami:</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="formula-tab" data-toggle="pill" href="#formula" role="tab" aria-controls="formula" aria-selected="true">1. FORMULA DEVELOPMENT</a>
                        <a class="nav-link" id="inspeksi-tab" data-toggle="pill" href="#inspeksi" role="tab" aria-controls="inspeksi" aria-selected="false">2. INSPEKSI ZAT TERLARANG</a>
                        <a class="nav-link" id="stabilitas-tab" data-toggle="pill" href="#stabilitas" role="tab" aria-controls="stabilitas" aria-selected="false">3. UJI STABILITAS & KOMPATIBILITAS</a>
                    </div>
                </div>

                <div class="col-lg-8 ml-auto">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="formula" role="tabpanel" aria-labelledby="formula-tab">
                            <div class="block_video">
                                <div class="about__info">
                                    <p>Kami merumuskan dan memproduksi produk perawatan kulit yang
                                        menjunjung tinggi standar kosmetik nasional yang menghasilkan
                                        hasil nyata dengan penggunaan bahan yang halal.
                                        Formulasi dari Konsep Karnus adalah konsep dasar bahwa bahan
                                        aktif yang digunakan dalam produk apa pun harus dapat
                                        memberikan khasiat, fungsi & manfaat yang efektif untuk solusi
                                        masalah kesehatan kulit serta sesuai dengan Algoritma dari Sang
                                        Pencipta.
                                        Kami memastikan bahwa klien kami menerima produk-produk
                                        berkualitas yang diformulasikan dari Konsep Karnus.
                                    </p>
                                    <h5>CUSTOM FORMULA</h5>
                                    <ul style="color: #6c7a87;">
                                        <li>Hingga 100% Pembandingan yang Akurat</li>
                                        <li>Memenuhi Kebutuhan dan Anggaran Anda</li>
                                        <li>Stabilitas & Kompatibilitas Diuji</li>
                                    </ul>
                                    <p>Kami menyediakan layanan formulasi khusus kepada pelanggan kami. Jika Anda
                                        memiliki konsep untuk produk perawatan kulit khusus seperti pembersih muka,
                                        exfoliator, pelembab, perawatan kerut atau produk lainnya-tim R&D kami dapat
                                        membuat formulasi sesuai dengan spesifikasi dan persyaratan Anda.</p>
                                    <p>Bergantung pada persyaratan formulasi yang diminta, kami dapat mengadaptasi
                                        salah satu produk dalam berbagai formulasi standar kami, atau menyesuaikan
                                        formula Anda dari awal. Semua keputusan ini didasarkan pada kebutuhan dan
                                        anggaran Anda. Jika Anda ingin produk yang mirip dengan produk yang telah ada di
                                        pasaran, R&D kami dapat melakukan pengembangan dan perbandingan dari produk
                                        referensi Anda tersebut.</p>
                                    <p>Jika Anda ingin mengembangkan formula khusus sendiri, sebaiknya lakukan riset
                                        untuk jenis produk yang Anda minati untuk memastikan kelayakannya. Sementara
                                        kami akan melakukan yang terbaik untuk memenuhi kebutuhan Anda dalam
                                        membangun produk Anda dari konsep-konsep yang digali. Fasilitas manufaktur
                                        kami menggunakan teknologi dan peralatan paling canggih untuk menghasilkan
                                        produk perawatan kulit terbaik.</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="inspeksi" role="tabpanel" aria-labelledby="inspeksi-tab">
                            <div class="block_video">
                                <div class="about__info">
                                    <p>Dalam proses pembuatan sample untuk produk Anda, tim R&D
                                        kami memeriksa dan memastikan bahan-bahan yang terkandung
                                        aman dan bebas dari bahan yang terlarang.</p>
                                    <!-- <a href="#">
                                        View Availability
                                        <i class="tio chevron_right"></i>
                                    </a> -->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="stabilitas" role="tabpanel" aria-labelledby="stabilitas-tab">
                            <div class="block_video">
                                <div class="about__info">
                                    <p>Tes Stabilitas dan Kompatibilitas untuk Produk Kosmetik
                                    </p>
                                    <p>Tujuan utama dari uji stabilitas dan kompatibilitas adalah untuk
                                        memastikan produk kosmetik tidak ada perubahan berbahaya
                                        dalam formulasi produk dan kualitas mircrobiological, serta
                                        keamanan dan kinerja fungsionalnya selama penanganan,
                                        transportasi dan penyimpanan dalam kondisi yang sesuai.
                                    </p>
                                    <p>Tim penguji memberikan informasi dengan memeriksa stabilitas
                                        produk kosmetik dalam rentang hidup yang sesuai dan
                                        kompatibilitas antara formulasi dan wadah bahan.</p>
                                    <h5>UJI STABILITAS</h5>
                                    <p>Studi Stabilitas Pengujian untuk Kosmetik
                                        meliputi evaluasi kualitas produk pada interval
                                        waktu tertentu dari periode penyimpanan
                                        dalam kondisi yang terkendali.</p>
                                    <p>Tujuan pengujian stabilitas produk kosmetik
                                        adalah untuk memastikan bahwa produk baru
                                        atau yang dimodifikasi memenuhi standar
                                        kualitas fisik, kimia dan mikrobiologi yang
                                        diharapkan serta fungsionalitas dan estetika
                                        bila disimpan dalam kondisi yang sesuai.</p>
                                    <h5>UJI KOMPATIBILITAS</h5>
                                    <p>Tes kompatibilitas kemasan dilakukan untuk
                                        menghindari masalah stabilitas formula dan
                                        masalah yang disebabkan oleh kemasan yang
                                        tidak sesuai kualifikasi.
                                    </p>
                                    <p>Tes kompatibilitas dilakukan dengan cara
                                        menyimpan kemasan produk dalam jangka
                                        waktu tertentu dengan kondisi yang
                                        terkontrol, kemudian dievaluasi karakteristik
                                        spesifiknya, seperti perubahan organoleptik,
                                        eksudasi, deformasi, kerusakan/pecah,
                                        masalah dalam fungsi dan aspek kemasan,
                                        lebih lanjut mikrobiologis dan stabilitas
                                        kimiawi konten.</p>
                                    <p>Tes mikroba memeriksa keberadaan
                                        mikroorganisme dalam suatu produk yang
                                        disediakan untuk laboratorium. Pengujian
                                        tersebut digunakan untuk keamanan produk,
                                        untuk mencari tanda-tanda kontaminasi pada
                                        produk yang akan dijual kepada publik, dan
                                        untuk kontrol laboratorium, untuk
                                        memastikan bahwa produk dan peralatan yang
                                        digunakan di laboratorium tidak
                                        terkontaminasi dengan mikroorganisme.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<?php

/** @var yii\web\View $this */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Tentang Kami';

?>

<!-- Start banner_about -->
<section class="pt_banner_inner banner_px_image">
    <div class="parallax_cover">
        <img class="cover-parallax h-100vh" src="<?= Yii::$app->homeUrl ?>web/maklon/header.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-6">
                <div class="banner_title_inner c-white">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        Tentang Kami
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100" class="banner_title_inner c-yollow">
                        Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End banner_about -->

<section class="pt_banner_inner banner_about_two padding-t-10">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-12">
                <div class="banner_title_inner">
                    <div class="before_title" data-aos="fade-up" data-aos-delay="0">
                        <span class="c-green2 font-w-500">Welcome 🖐</span>
                    </div>
                    <h1 data-aos="fade-up" data-aos-delay="100">
                        PT Alga Rosan Nusantara
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="200">
                        Perusahaan Marketing & Sales dari Raissa Beauty Grup
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid pl-0">
        <div class="row">
            <div class="col-lg-12">
                <div class="parallax_cover">

                </div>
            </div>
        </div>
    </div>

    <div class="container padding-t-6">
        <div class="row">
            <div class="col-lg-6">
                <p class="font-s-18 c-gray">
                    Masa depan bisnis nutrisi & beauty di Indonesia masih sangat menjanjikan. Karena Sebagian besar
                    masyarakat telah menjadikan produk kesehatan dan kosmetik sebagai kebutuhan primer.
                    PT Alga Rosan Nusantara adalah perusahaan marketing & sales untuk nutrisi & beauty yang juga
                    melayani jasa pembuatan produk nutrisi & beauty dengan nama brand Anda, yang dibangun sebagai
                    bentuk urgensi dari industri kesehatan di Indonesia yang membutuhkan banyak produk lokal yang
                    dapat bersaing secara global.
                    Kami juga melayani perizinan produk kosmetik BPOM dan pengakuan HAKI. Melaui riset ilmiah dan
                    research & development (R&D), kami membuat produk sesuai yang Anda inginkan tanpa menyimpang
                    dari medis & manipulasi.
                    Kami melayani proses manufaktur dari hulu hingga hilir, mulai dari research & development, formulasi
                    bahan baku, quality control, pengemasan, hingga pengantaran produk ke warehouse Anda.
                    Perusahaan kami dilengkapi dengan fasilitas produksi canggih dan memadai sehingga mutu produk
                    anda dijamin Halal, aman, dan tidak berbahaya bagi kesehatan Anda di masa depan.
                </p>
            </div>
            <div class="col-lg-5 ml-auto">
                <p class="font-s-18 c-gray">
                    Kami memiliki profesionalisme terpercaya di bidang manufaktur produk jasa maklon nutrisi & beauty.
                    Perusahaan kami telah tersertifikasi GMP, BPOM, ISO 9001, dan Halal dari MUI. Sehingga pelayanan
                    area jasa maklon kami mencakup nasional dan ke luar negeri.
                    Motivasi kami sangat besar untuk memproduksi produk yang tidak hanya sesuai dengan trend dan
                    keinginan para customer, tapi juga memberikan keuntungan baik bisnis ke bisnis, maupun bisnis ke
                    konsumen serta di formulasikan oleh Konsep Karnus. Sebuah konsep yang kesehatan yang sesuai
                    dengan Algoritma dari Sang Pencipta.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="service__workspace padding-py-6 mt-0 w-100">
    <div class="container">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="<?= Yii::$app->homeUrl ?>web/maklon/homepage-client-established.webp" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?= Yii::$app->homeUrl ?>web/maklon/homepage-client-established.webp" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?= Yii::$app->homeUrl ?>web/maklon/homepage-client-established.webp" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>


        <div class="row justify-content-center mt-5">
            <div class="col-lg-7">
                <div class="form_cc_four" data-aos="fade-up" data-aos-delay="300">

                    <h3 class="mb-4">Untuk mengetahui info dan detail perusahaan kami secara lebih detil, silahkan hubungi kami.</h3>

                    <?= Alert::widget() ?>

                    <form action="<?= Url::current() ?>" class="row mt-5" method="post">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" class="form-control" name="DownloadFile[nama]" value="<?= $model->nama ?>" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="DownloadFile[email]" value="<?= $model->email ?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>No. Telp. / HP</label>
                                <input type="tel" class="form-control" name="DownloadFile[hp]" value="<?= $model->hp ?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control" rows="7" name="DownloadFile[keterangan]"><?= $model->keterangan ?></textarea>
                            </div>
                        </div>

                        <div class="col-12 d-md-flex justify-content-end margin-t-2">
                            <button type="submit" class="btn btn_md_primary sweep_top sweep_letter h-fit-content bg-orange-red rounded-8 c-white">
                                <div class="inside_item">
                                    <span data-hover="Download">Download File</span>
                                </div>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>
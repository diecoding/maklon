<?php

/** @var yii\web\View $this */

use app\assets\AppAsset;
use yii\helpers\Url;

$this->title = 'Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara';

?>

<!-- Start Banner Section -->
<section class="demo_1 banner_section banner_demo8 border-bottom-1">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-5 order-2 order-lg-1">
                <div class="banner_title text-center text-sm-left">
                    <h1 class="c-blue">
                        Maklon Nutrisi
                        <span class="c-yollow">& Beauty</span>
                    </h1>
                    <h5 class="mb-5">
                        Punya ide bisnis nutrisi kesehatan, kosmetik atau skincare tapi bingung harus mulai dari mana? <br><br>
                        <strong class="c-blue">PT Alga Rosan Nusantara bisa membantu Anda membuat produk sesuai ide & cita-cita Anda!</strong>
                    </h5>
                    <div class="subscribe_phone">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="button--click">
                                    <a href="<?= whatsappUrl() ?>" class="btn btn_md_primary sweep_top sweep_letter bg-orange-red c-white rounded-8 d-block">
                                        <div class="inside_item">
                                            <span data-hover="<?= phonePretty() ?>">Hubungi Kami</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-5 text-center text-sm-left">
                        <a href="#Inovasi" class="h4 font-weight-bold d-block c-orange-red">#Maklon<span class="c-gold">Nutrisi</span></a>
                        <a href="#Inovasi" class="h4 font-weight-bold d-block c-blue">#Maklon<span class="c-orange">Beauty</span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 mx-auto order-1 order-lg-1">
                <div class="ill_appMobile">
                    <!-- <img class="ill_app" src="<?php // Yii::$app->homeUrl ?>web/img/app/c_app.svg" /> -->
                    <img class="ill_bg" src="<?= Yii::$app->homeUrl ?>web/img/app/background.svg" />
                    <img class="ill_user" src="<?= Yii::$app->homeUrl ?>web/maklon/produk_maklon.webp" />
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Banner -->


<section class="d-block d-md-flex align-items-top">
    <div class="d-block w-100">
        <img class="ill_user w-100" src="<?= Yii::$app->homeUrl ?>web/maklon/homepage-client-established.webp" />
        <div class="d-block d-md-flex  align-items-center justify-content-between p-5">

            <h5 class="mb-3 mb-md-0 w-md-75 me-md-2">
                Ingin produk Anda jadi tren dan brand Anda saat ini jadi besar? Namun belum punya sistem online yang canggih? Lihat bagaimana kami memberikan Anda jalan keluar.
            </h5>

            <a href="<?= whatsappUrl("[Sudah Punya Bisnis] Hallo Kak, saya sudah punya bisnis & mau tanya-tanya dong :)\n") ?>" class="btn btn_md_primary sweep_top sweep_letter bg-orange-red c-white rounded-8">
                Sudah Punya Bisnis
            </a>
        </div>
    </div>


    <div class="d-block w-100">
        <img class="ill_user w-100" src="<?= Yii::$app->homeUrl ?>web/maklon/homepage-client-startup.webp" />
        <div class="d-block d-md-flex  align-items-center justify-content-between p-5">

            <h5 class="mb-3 mb-md-0 w-md-75 me-md-2">
                Kami pandu jika Anda baru memulai bisnis di bidang Nutrisi, Kosmetik & Skincare.
            </h5>

            <a href="<?= whatsappUrl("[Belum Punya Bisnis] Hallo Kak, saya belum punya bisnis & mau tanya-tanya dong :)\n") ?>" class="btn btn_md_primary sweep_top sweep_letter bg-orange-red c-white rounded-8">
                Belum Punya Bisnis
            </a>
        </div>
    </div>
</section>


<section class="services_section bg-yollow my-0 padding-py-12 padding-px-12 text-center">
    <h1 class="c-primary"><strong>PEMBERITAHUAN</strong></h1>
    <h4 class="c-primary">
        Hati-hati terhadap informasi yang mengatasnamakan PT Alga Rosan Nusantara.<br>
        Selalu cek informasi di website resmi kami atau langsung bertanya ke CS.
    </h4>
</section>

<section class="service__workspace padding-py-12 bg-white">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-5">
                <div class="title_sections_inner margin-b-10">
                    <h2>ONE STOP SERVICE MAKLON</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4 block__item mb-4 mb-lg-0">
                <div class="inside__zoop">
                    <div class="media">
                        <div class="ico">
                            <i class="tio test-tube"></i>
                        </div>
                        <div class="media-body">
                            <div class="t_xt d-flex justify-content-between">
                                <h4>Riset</h4>
                                <i class="tio arrow_forward my-auto c-gray font-s-18 d-none d-lg-block"></i>
                                <span class=" my-auto c-gray font-s-18 d-block d-lg-none">1</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 block__item mb-4">
                <div class="inside__zoop">
                    <div class="media">
                        <div class="ico">
                            <i class="tio lab"></i>
                        </div>
                        <div class="media-body">
                            <div class="t_xt d-flex justify-content-between">
                                <h4>Formula</h4>
                                <i class="tio arrow_forward my-auto c-gray font-s-18 d-none d-lg-block"></i>
                                <span class=" my-auto c-gray font-s-18 d-block d-lg-none">2</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 block__item">
                <div class="inside__zoop">
                    <div class="media">
                        <div class="ico">
                            <i class="tio color"></i>
                        </div>
                        <div class="media-body">
                            <div class="t_xt d-flex justify-content-between">
                                <h4>Design</h4>
                                <i class="tio arrow_forward my-auto c-gray font-s-18 d-none d-lg-block"></i>
                                <span class=" my-auto c-gray font-s-18 d-block d-lg-none">3</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row mt-4">
            <div class="col-md-6 col-lg-4 block__item mb-4 mb-lg-0">
                <div class="inside__zoop">
                    <div class="media">
                        <div class="ico">
                            <i class="tio dropbox"></i>
                        </div>
                        <div class="media-body">
                            <div class="t_xt d-flex justify-content-between">
                                <h4>Packaging</h4>
                                <i class="tio arrow_forward my-auto c-gray font-s-18 d-none d-lg-block"></i>
                                <span class=" my-auto c-gray font-s-18 d-block d-lg-none">4</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 block__item mb-4">
                <div class="inside__zoop">
                    <div class="media">
                        <div class="ico">
                            <i class="tio file_add_outlined"></i>
                        </div>
                        <div class="media-body">
                            <div class="t_xt d-flex justify-content-between">
                                <h4>Permit</h4>
                                <i class="tio arrow_forward my-auto c-gray font-s-18 d-none d-lg-block"></i>
                                <span class=" my-auto c-gray font-s-18 d-block d-lg-none">5</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 block__item">
                <div class="inside__zoop">
                    <div class="media">
                        <div class="ico">
                            <i class="tio shop_outlined"></i>
                        </div>
                        <div class="media-body">
                            <div class="t_xt d-flex justify-content-between">
                                <h4>Ecommerce</h4>
                                <i class="tio checkmark_circle_outlined my-auto c-gray font-s-18 d-none d-lg-block"></i>
                                <span class=" my-auto c-gray font-s-18 d-block d-lg-none">6</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="feature_dem3 padding-py-12">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-md-8 col-lg-6">
                <div class="title_sections">
                    <h2>Mengapa memilih Kami?</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4 Oitem margin-b-5">
                <div class="item_feth aos-init" data-aos="fade-up" data-aos-delay="0">
                    <div class="media">
                        <div class="icon_fr">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/Dial-numbers.svg">
                        </div>
                        <div class="media-body">
                            <div class="za_tzt">
                                <h3>R&D Handal</h3>
                                <p>
                                    Kami mampu mewujudkan konsep nutrisi & beauty skincare Anda menjadi produk yang bisa dipasarkan & diterima oleh konsumen.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 Oitem margin-b-5">
                <div class="item_feth aos-init" data-aos="fade-up" data-aos-delay="100">
                    <div class="media">
                        <div class="icon_fr">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/Crown.svg">
                        </div>
                        <div class="media-body">
                            <div class="za_tzt">
                                <h3>Perizinan Mudah</h3>
                                <p>
                                    Kami bantu proses perizinan mengurus BPOM, sertifikasi halal MUI, dan sertifikasi merek dan paten. Produk Anda legal, halal, dan aman.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 Oitem margin-b-5">
                <div class="item_feth aos-init" data-aos="fade-up" data-aos-delay="200">
                    <div class="media">
                        <div class="icon_fr">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/Cloud-upload.svg">
                        </div>
                        <div class="media-body">
                            <div class="za_tzt">
                                <h3>Formula Unik</h3>
                                <p>
                                    Produk Anda tidak akan sama dengan brand lain yang ada di pasaran. Formula yang kami buat untuk Anda, unik dan spesial. Kami produsen kolagen sapi halal satu-satunya se-Asia Tenggara saat ini.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 Oitem">
                <div class="item_feth aos-init" data-aos="fade-up" data-aos-delay="0">
                    <div class="media">
                        <div class="icon_fr">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/Settings-white.svg">
                        </div>
                        <div class="media-body">
                            <div class="za_tzt">
                                <h3>Jaminan Kualitas</h3>
                                <p>
                                    Kami hasilkan produk berkualitas sesuai standar mutu yang telah ditetapkan secara nasional dan internasional.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 Oitem">
                <div class="item_feth aos-init" data-aos="fade-up" data-aos-delay="100">
                    <div class="media">
                        <div class="icon_fr">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/Pen&amp;ruller.svg">
                        </div>
                        <div class="media-body">
                            <div class="za_tzt">
                                <h3>Support System</h3>
                                <p>
                                    Kami bantu Anda untuk membuat toko online yang lengkap sehingga konsumen Anda sangat nyaman berbelanja produk Anda.

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 Oitem d-none">
                <div class="item_feth aos-init" data-aos="fade-up" data-aos-delay="200">
                    <div class="media">
                        <div class="icon_fr">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/Headphones-white.svg">
                        </div>
                        <div class="media-body">
                            <div class="za_tzt">
                                <h3></h3>
                                <p>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="logos_section logos_demo3 padding-py-8 text-center mt-0">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-md-8 col-lg-6">
                <div class="title_sections">
                    <h2>Produk Yang Diproduksi & Diedarkan oleh PT Alga Rosan Nusantara</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-12">
                <div class="items_loog">
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/j1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/w1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/o1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/h1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/n1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/j1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/w1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/o1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/h1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/n1.png" alt="" width="200">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="logos_section logos_demo3 padding-t-8 text-center mt-0 border-t-2">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-md-8 col-lg-6">
                <div class="title_sections">
                    <h2>Brand Yang Maklon di PT Alga Rosan Nusantara</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-12">
                <div class="items_loog">
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/j1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/w1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/o1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/h1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/n1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/j1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/w1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/o1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/h1.png" alt="" width="200">
                    </div>
                    <div class="item-client mb-3">
                        <img src="<?= Yii::$app->homeUrl ?>web/img/logos/n1.png" alt="" width="200">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="simplecontact_section bg-white padding-py-12">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="title_sections mb-1 mb-sm-auto">
                    <h2>Buat Jadwal Kunjungan</h2>
                    <p>
                        Tim kami akan segera menghubungi Anda untuk konfirmasi ulang.
                        Jam operasional kami di hari Senin sampai Jumat dari jam 09.00 pagi hingga pukul 17.00 sore.
                    </p>
                </div>
            </div>
            <div class="col-md-6 my-auto ml-auto text-sm-right">
                <a href="<?= Url::toRoute(['/site/kontak-kami', '#' => 'buat-kunjungan']) ?>" class="btn mt-3 rounded-12 sweep_top sweep_letter btn_md_primary c-white scale bg-green2">
                    <div class="inside_item">
                        <span data-hover="Jadwalkan Kunjungan">Buat Jadwal Kunjungan</span>
                    </div>
                </a>
            </div>
        </div>
        <div class="circle-ripple z-index-0 d-none d-sm-block">
            <div class="ripple ripple-1"></div>
            <div class="ripple ripple-2"></div>
            <div class="ripple ripple-3"></div>
        </div>
    </div>
</section>
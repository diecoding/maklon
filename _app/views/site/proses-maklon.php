<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'Proses Maklon';

?>

<!-- Start banner_about -->
<section class="pt_banner_inner banner_px_image">
    <div class="parallax_cover">
        <img class="cover-parallax h-100vh" src="<?= Yii::$app->homeUrl ?>web/maklon/header.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-6">
                <div class="banner_title_inner c-white">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        Proses Maklon
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100" class="banner_title_inner c-yollow">
                        Maklon Nutrisi & Beauty - PT Alga Rosan Nusantara
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End banner_about -->

<!-- Start Services -->
<section class="services_section padding-t-10">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <div class="title_sections_inner margin-b-5">
                    <h2>Proses Maklon Nutrisi dan Beauty Kami
                        memberikan layanan lengkap bagi Anda yang
                        baru memulai bisnis produk kecantikan.
                        Langkahnya mudah. Kami bersama Anda di sepanjang prosesnya.
                    </h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="items_serv">
                    <div class="media">
                        <div class="item-img">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/phone.svg" alt="" />
                        </div>
                        <div class="media-body">
                            <div class="txt-small">
                                <span>Step 1</span>
                            </div>
                            <h3>Konsultasi Konsep Produk</h3>
                            <p>Berikan kami gambaran mengenai produk yang
                                ingin dibuat. Tidak perlu khawatir, jika Anda
                                masih merasa ragu atau bingung mengenai
                                konsep produk. Tim marketing kami akan
                                memberikan saran dari pemilihan formula,
                                bahan baku kosmetik, tekstur & kemasan.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="items_serv">
                    <div class="media">
                        <div class="item-img">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/name.svg" alt="" />
                        </div>
                        <div class="media-body">
                            <div class="txt-small">
                                <span>Step 2</span>
                            </div>
                            <h3>Pembuatan Sampel Produk</h3>
                            <p>Setelah menentukan produk, formula, dan
                                bahan baku yang telah disetujui bersama, kami
                                akan membuatkan sampel produk. Pada tahap
                                ini produk akan melalui uji stabilitas dan
                                kompatibilitas.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="items_serv">
                    <div class="media">
                        <div class="item-img">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/discount.svg" alt="" />
                        </div>
                        <div class="media-body">
                            <div class="txt-small">
                                <span>Step 3</span>
                            </div>
                            <h3>Lengkapi Dokumen</h3>
                            <p>Dokumen yang diperlukan untuk proses maklon
                                adalah KTP, NPWP (bila ada), dan fotokopi
                                SIUP anda yang berbadan usaha.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="services_section padding-t-10">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="items_serv">
                    <div class="media">
                        <div class="item-img">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/phone.svg" alt="" />
                        </div>
                        <div class="media-body">
                            <div class="txt-small">
                                <span>Step 4</span>
                            </div>
                            <h3>Kontrak Kerjasama Maklon</h3>
                            <p>Jika Anda telah menyetujui sampel produk,
                                Anda akan menandatangani kontrak maklon dan
                                telah menyepakati hal-hal seperti jumlah
                                produk, formula dan bahan-bahan baku,
                                kemasan, harga pokok penjualan produk, dan
                                biaya jasa maklon.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="items_serv">
                    <div class="media">
                        <div class="item-img">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/name.svg" alt="" />
                        </div>
                        <div class="media-body">
                            <div class="txt-small">
                                <span>Step 5</span>
                            </div>
                            <h3>Pembayaran Biaya Maklon</h3>
                            <p>Setelah membuat kesepakatan, Anda akan
                                melanjutkan ke proses pembayaran. Biaya
                                maklon yang harus dibayarkan dimuka / DP
                                adalah sebesar 50% dari total pemesanan
                                produk.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="items_serv">
                    <div class="media">
                        <div class="item-img">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/discount.svg" alt="" />
                        </div>
                        <div class="media-body">
                            <div class="txt-small">
                                <span>Step 6</span>
                            </div>
                            <h3>Mengurus Perizinan</h3>
                            <p>Kami akan mengurus semua perizinan yang
                                diperlukan, mulai dari registrasi BPOM,
                                sertifikasi Halal dari LPPOM MUI, dan
                                sertifikasi hak kekayaan intelektual seperti
                                merek dan hak paten.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="services_section padding-t-10">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="items_serv">
                    <div class="media">
                        <div class="item-img">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/phone.svg" alt="" />
                        </div>
                        <div class="media-body">
                            <div class="txt-small">
                                <span>Step 7</span>
                            </div>
                            <h3>Mulai Produksi</h3>
                            <p>Produksi akan dimulai setelah nomor BPOM
                                untuk produk sudah keluar. Kami menjamin
                                keamanan dan kehalalan produk dalam setiap
                                proses pembuatan.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="items_serv">
                    <div class="media">
                        <div class="item-img">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/name.svg" alt="" />
                        </div>
                        <div class="media-body">
                            <div class="txt-small">
                                <span>Step 8</span>
                            </div>
                            <h3>Kualitas Kontrol</h3>
                            <p>Setelah diproduksi, produk harus melewati
                                proses kualitas kontrol untuk memenuhi
                                standar kualitas yang sesuai dengan standar
                                mutu yang telah ditetapkan.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                <div class="items_serv">
                    <div class="media">
                        <div class="item-img">
                            <img src="<?= Yii::$app->homeUrl ?>web/img/icons/discount.svg" alt="" />
                        </div>
                        <div class="media-body">
                            <div class="txt-small">
                                <span>Step 9</span>
                            </div>
                            <h3>Pengiriman Produk</h3>
                            <p>Setelah proses produksi selesai, kami akan
                                membantu mengirimkan produk Anda yang siap
                                dijual ke warehouse Anda.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Services -->

<section class="service__workspace features__workspace padding-py-12" id="Services">
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-lg-12">
                <div class="title_sections_inner mb-0">
                    <div class="before_title">
                        <span class="c-orange-red">Satu Langkah mudah</span>
                    </div>
                    <h2>Ceritakan konsep produk Anda sekarang. Kami
                        akan membantu mengembangkan produk Anda.
                        Hubungi kami untuk informasi lebih lanjut.</h2>
                    <a href="" class="btn btn_md_primary sweep_top sweep_letter bg-orange-red c-white rounded-8">
                        <div class="inside_item">
                            <span data-hover="Buat Produk Maklon">Buat Produk Maklon</span>
                        </div>
                    </a>
                    <a href="" class="btn btn_md_primary sweep_top sweep_letter bg-orange-red c-white rounded-8">
                        <div class="inside_item">
                            <span data-hover="Konsultasi Lebih lanjut">Konsultasi Lebih lanjut</span>
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
</section>
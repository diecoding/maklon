<?php

/** @var yii\web\View $this */

use app\widgets\Alert;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Kontak Kami';

?>

<!-- Start banner_cotact_one -->
<section class="pt_banner_inner banner_cotact_one banner_cotact_four" id="myNavbar">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-md-8 col-lg-6">
                <div class="banner_title_inner margin-b-5">
                    <h1 class="c-dark" data-aos="fade-up" data-aos-delay="0">
                        Hi, 🖐 we are <span class="c-orange-red">Maklon.</span>
                    </h1>
                    <p class="c-gray" data-aos="fade-up" data-aos-delay="100">
                        Kami menanggapi dengan serius semua umpan balik yang kami terima dari para konsumen kami.
                        Peran serta klien sangat kami butuhkan dalam membangun layanan yang berkualitas untuk memenuhi semua keinginan para klien kami.
                        <br>
                        Jika anda masih memiliki pertanyaan yang belum terjawab, kami dapat memberikan layanan konsultasi gratis bagi anda.
                        Jangan ragu hubungi kami, tim marketing kami sangat terbuka dan siap melayani semua kebutuhan anda akan produk
                        nutrisi & kosmetik yang berkualitas.
                    </p>
                </div>
            </div>
        </div>


        <div id="buat-kunjungan"></div>

        <div class="row justify-content-center text-center">
            <div class="col-md-8 col-lg-5">
                <div class="title_sections_inner">
                    <h2>Buat Jadwal Kunjungan</h2>

                    <p class="c-gray" data-aos="fade-up" data-aos-delay="100">
                        Silahkan isi formulir berikut ini dan sertakan informasi besaran budget, rincian produk yang ingin dibuat, serta referensi produk.
                        Tanggal Berkunjung diisi minimal H+5 dari waktu pembuatan form ini. Tim kami akan segera menghubungi Anda untuk konfirmasi ulang. Untuk berbicara dengan Customer Service kami, silahkan klik link Whatsapp di bawah ini.
                        Jam operasional kami di hari Senin sampai Jumat dari jam 09.00 pagi hingga pukul 17.00 sore.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End. Banner -->

<!-- Start section_contact_four -->
<section class="section_contact_four">
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="form_cc_four" data-aos="fade-up" data-aos-delay="300">

                    <?= Alert::widget() ?>

                    <form action="<?= Url::current() ?>" class="row mt-5" method="post">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" class="form-control" name="Contact[nama]" value="<?= $model->nama ?>" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="Contact[email]" value="<?= $model->email ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <select class="form-control custom-select" name="Contact[jenis_kelamin]" value="<?= $model->jenis_kelamin ?>">
                                    <option value="">Pilih</option>
                                    <option value="Laki-laki" <?= $model->jenis_kelamin === 'Laki-laki' ? 'selected' : '' ?>>Laki-laki</option>
                                    <option value="Perempuan" <?= $model->jenis_kelamin === 'Perempuan' ? 'selected' : '' ?>>Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>No. Telp. / HP</label>
                                <input type="tel" class="form-control" name="Contact[hp]" value="<?= $model->hp ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Berkunjung</label>
                                <input type="date" class="form-control" name="Contact[tanggal_berkunjung]" value="<?= $model->tanggal_berkunjung ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Budget</label>
                                <select class="form-control custom-select" name="Contact[budget]" value="<?= $model->budget ?>">
                                    <option value="">Pilih</option>
                                    <option value="100 - 250 Juta" <?= $model->budget === '100 - 250 Juta' ? 'selected' : '' ?>>100 - 250 Juta</option>
                                    <option value="250 - 500 Juta" <?= $model->budget === '250 - 500 Juta' ? 'selected' : '' ?>>250 - 500 Juta</option>
                                    <option value="> 500 Juta" <?= $model->budget === '> 500 Juta' ? 'selected' : '' ?>>> 500 Juta</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Domisili</label>
                                <textarea class="form-control" rows="4" name="Contact[domisili]"><?= $model->domisili ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control" rows="7" name="Contact[keterangan]"><?= $model->keterangan ?></textarea>
                            </div>
                        </div>

                        <div class="col-12 d-md-flex justify-content-end margin-t-2">
                            <button type="submit" class="btn btn_md_primary sweep_top sweep_letter h-fit-content bg-orange-red rounded-8 c-white">
                                <div class="inside_item">
                                    <span data-hover="Submit">Jadwalkan Kunjungan</span>
                                </div>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End. section_contact_four -->


<!-- Start sec__office_location -->
<section class="sec__office_location pt-5" id="contact">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-md-8 col-lg-5">
                <div class="title_sections_inner">
                    <h2>Office Locations</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End. sec__office_location -->

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.320401160943!2d112.72422091144993!3d-7.429751873163655!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7e75fdb2085f3%3A0xb5d4ae1579fe2c62!2sPT.Alga%20Rosan%20Nusantara!5e0!3m2!1sen!2sid!4v1686329615763!5m2!1sen!2sid" style="border:0; width: 100%; height: 480px;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
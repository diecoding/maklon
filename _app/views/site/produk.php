<?php

/** @var yii\web\View $this */

use app\widgets\Alert;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Produk';


// $this->registerCssFile('https://nose.co.id/src/css/styles.css');
// $this->registerCssFile('https://nose.co.id/src/css/custom.css');
?>

<!-- Start banner_cotact_one -->
<section class="pt_banner_inner banner_cotact_one banner_cotact_four pb-5">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-md-8 col-lg-6">
                <div class="banner_title_inner margin-b-5">
                    <h1 class="c-dark" data-aos="fade-up" data-aos-delay="0">
                        Buat Produk <span class="c-orange-red">Kecantikan.</span>
                    </h1>
                    <p class="c-gray" data-aos="fade-up" data-aos-delay="100">
                        Cari tahu minimum order quantity (MOQ) produk yang ingin anda buat.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End. Banner -->

<section class="container">

    <div class="row">

        <?php

        $products = [
            'SKINCARE' => [
                'options' => [
                    'class' => 'col-12 col-lg-12',
                    'img' => Yii::$app->homeurl . 'web/maklon/skincare.png',
                ],
                'items' => [
                    'Cleanser' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-4',
                        ],
                        'items' => [
                            ['Micellar Water', '#'],
                            ['Cleansing Water', '#'],
                            ['Cleansing Oil', '#'],
                            ['Cleansing Balm', '#'],
                            ['Cleansing Lotion', '#'],
                            ['Cleansing Cream', '#'],
                            ['Milk Cleanser', '#'],
                            ['Facial Wash', '#'],
                        ],
                    ],
                    'Serum' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-4',
                        ],
                        'items' => [
                            ['Serum Anti Acne (Jerawat)', '#'],
                            ['Serum Anti Aging (Penuaan)', '#'],
                            ['Serum Whitening (Pencerah Wajah)', '#'],
                            ['Serum Vitamin C', '#'],
                            ['Serum Pelembab', '#'],
                            ['Serum Antioksidan', '#'],
                            ['Serum Eksfoliasi', '#'],
                        ],
                    ],
                    'Moisturizer' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-4',
                        ],
                        'items' => [
                            ['Day Cream', '#'],
                            ['Night Cream', '#'],
                            ['Face Oil', '#'],
                            ['Face Mist', '#'],
                            ['Sun Protection', '#'],
                        ],
                    ],
                    'Treatment' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-6',
                        ],
                        'items' => [
                            ['Eye Treatment', '#'],
                            ['Brow & Lash Treatment', '#'],
                            ['Soothing Treatment', '#'],
                            ['Acne Treatment', '#'],
                            ['Essence', '#'],
                            ['Peeling', '#'],
                            ['Scrub & Exfoliator', '#'],
                            ['Toner', '#'],
                        ],
                    ],
                    'Masker' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-6',
                        ],
                        'items' => [
                            ['Sheet Mask', '#'],
                            ['Clay Mask', '#'],
                            ['Peel Off Mask', '#'],
                            ['Sleeping Mask', '#'],
                            ['Eye Mask', '#'],
                        ],
                    ],
                ],

            ],

            'BODY CARE' => [
                'options' => [
                    'class' => 'col-12 col-lg-12',
                    'img' => Yii::$app->homeurl . 'web/maklon/bodycare.png',
                ],
                'items' => [
                    'Body Moisturizer' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-4',
                        ],
                        'items' => [
                            ['Body Lotion', '#'],
                            ['Body Butter', '#'],
                            ['Body Cream', '#'],
                            ['Body Serum', '#'],
                            ['Body Oil', '#'],
                            ['Body Gel', '#'],
                            ['Body Mask', '#'],
                        ],
                    ],
                    'Hand & Foot Care' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-4',
                        ],
                        'items' => [
                            ['Hand Wash', '#'],
                            ['Hand Sanitizer', '#'],
                            ['Hand Cream', '#'],
                            ['Foot Cream', '#'],
                            ['Foot Spray', '#'],
                        ],
                    ],
                    'Personal Care' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-4',
                        ],
                        'items' => [
                            ['Deodorant', '#'],
                            ['Hair Removal Cream', '#'],
                            ['Shaving Cream', '#'],
                            ['Feminine Wash', '#'],
                            ['Essential Oil', '#'],
                        ],
                    ],
                    'Body Treatment' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-6',
                        ],
                        'items' => [
                            ['Cellulite Cream', '#'],
                            ['Stretch Marks Cream', '#'],
                            ['Breast Cream', '#'],
                            ['Slimming Cream', '#'],
                        ],
                    ],
                    'Body Cleanser' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-6',
                        ],
                        'items' => [
                            ['Body Wash', '#'],
                            ['Body Scrub & Exfoliator (Lulur)', '#'],
                        ],
                    ],
                ],

            ],

            'HAIR CARE' => [
                'options' => [
                    'class' => 'col-12 col-lg-8',
                    'img' => Yii::$app->homeurl . 'web/maklon/haircare.png',
                ],
                'items' => [
                    'Hair Cleanser ' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-4',
                        ],
                        'items' => [
                            ['Shampoo', '#'],
                            ['Conditioner', '#'],
                        ],
                    ],
                    'Styling & Color' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-4',
                        ],
                        'items' => [
                            ['Pomade', '#'],
                            ['Hair Gel', '#'],
                            ['Heat Protectant', '#'],
                        ],
                    ],
                    'Hair Treatment' => [
                        'options' => [
                            'class' => 'col-12 col-sm-6 col-lg-4',
                        ],
                        'items' => [
                            ['Hair Serum', '#'],
                            ['Hair Vitamin', '#'],
                            ['Hair Tonic', '#'],
                            ['Hair Oil', '#'],
                            ['Hair Creambath', '#'],
                            ['Hair Spa', '#'],
                            ['Hair Mask', '#'],
                            ['Hair Ampoule', '#'],
                            ['Hair Essence', '#'],
                            ['Hair Mist', '#'],
                        ],
                    ],
                ],

            ],

            'PERFUME' => [
                'options' => [
                    'class' => 'col-12 col-lg-4',
                    'img' => Yii::$app->homeurl . 'web/maklon/parfum.png',
                ],
                'items' => [
                    '' => [
                        'options' => [
                            'class' => 'col-12',
                        ],
                        'items' => [
                            ['Eau De Parfum', '#'],
                            ['Eau De Toilette', '#'],
                            ['Eau De Cologne', '#'],
                            ['Body Mist', '#'],
                            ['Perfume Oil', '#'],
                        ],
                    ],
                ],

            ],
        ];

        foreach ($products as $title => $product) {

            $optionsProduct = $product['options'];
            $subProducts = $product['items'];

        ?>

            <div class="<?= $optionsProduct['class'] ?>">
                <div class="faq_one_inner my-0 w-10 py-5">

                    <div class="py-4">
                        <h2 class="fw-bold mb-4"><?= $title ?></h2>

                        <div class="mt-4 text-left">
                            <img src="<?= $optionsProduct['img'] ?>" alt="" class="w-100" style="max-width: 400px;">
                        </div>

                        <div class="row">

                            <?php foreach ($subProducts as $subtitle => $data) {
                                $options = $data['options'];
                                $items = $data['items'];
                            ?>
                                <div class="<?= $options['class'] ?> mt-2">

                                    <div class="features_points">
                                        <h4 class="mt-5 mb-3"><em><?= $subtitle ?></em></h4>

                                        <ul class="list-group list_feat">

                                            <?php foreach ($items as [$label, $url]) { ?>
                                                <li class="list-group-item border-0 mt-2 mb-0">
                                                    <a href="<?= $url ?>">
                                                        <i class="tio checkmark_circle_outlined text-dark"></i>
                                                        <span class="text-dark ms-2"><?= $label ?></span>
                                                    </a>
                                                </li>
                                            <?php } ?>

                                        </ul>
                                    </div>

                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>
    </div>

</section>

<!-- Produk Lainnya -->
<section class="bg-white py-5">
    <div class="container text-center ">
        <h1 class="font-bold fs-1">Mari Diskusi Lebih Lanjut</h1>
        <p class="fs-4">
            Jika kamu ingin tau lebih lanjut bagaimana kamu bisa segera memulai membuat produk impianmu,
            silahkan hubungi kami.
        </p>
    </div>
</section>


<!-- Start banner_cotact_one -->
<section class="pt_banner_inner banner_cotact_one banner_cotact_four pb-5 padding-t-10">
    <div class="container">
        <div class="text-center">
                <div class="banner_title_inner">
                    <h1 class="c-dark" data-aos="fade-up" data-aos-delay="0">
                        NUTRISI
                    </h1>
            </div>
        </div>
    </div>
</section>
<!-- End. Banner -->


<section class="container padding-py-5 text-center">

    <div class="features_points">
        <h4 class="mt-5 mb-3">Collagen Drink</h4>

        <ul class="list-group list_feat">

            <li class="list-group-item border-0 mt-2 mb-0">
                <i class="tio checkmark_circle_outlined text-dark"></i>
                <span class="text-dark ms-2">Untuk Kesehatan Kulit</span>
            </li>
            <li class="list-group-item border-0 mt-2 mb-0">
                <i class="tio checkmark_circle_outlined text-dark"></i>
                <span class="text-dark ms-2">Untuk Kesehatan Otot & Tulang</span>
            </li>
            <li class="list-group-item border-0 mt-2 mb-0">
                <i class="tio checkmark_circle_outlined text-dark"></i>
                <span class="text-dark ms-2">Kopi/Rempah</span>
            </li>

        </ul>
    </div>

    <div class="features_points">
        <h4 class="mt-5 mb-3">Anti Oxidance</h4>
    </div>

    <div class="features_points">
        <h4 class="mt-5 mb-3">Slimming</h4>
    </div>

</section>
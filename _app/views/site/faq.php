<?php

/** @var yii\web\View $this */

use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Frequently Asked Questions (FAQ)';

?>


<!-- Start banner_about -->
<section class="pt_banner_inner banner_about_three pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-6">
                <div class="banner_title_inner margin-b-3">
                    <h1 data-aos="fade-up" data-aos-delay="0">
                        FAQ
                    </h1>
                    <p data-aos="fade-up" data-aos-delay="100">
                        Frequently Asked Questions
                    </p>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- End banner_about -->

<!-- Start FAQ -->
<section class="faq_section faq_demo5 bg-white bg-white position-relative z-index-3 py-5 mb-5" id="faq">
    <div class="container">

        <!-- block Collapse -->
        <div class="block_faq">
            <div class="accordion" id="accordionExample">
                <div class="row justify-content-md-center">
                    <div class="col-md-9 col-lg-7">

                        <?php

                        $data = [
                            [
                                'Mengapa saya harus memilih PT ARN dalam membuat produk kosmetik dan skincare?',
                                'Kami menjamin proses pembuatan produk nutrisi & kosmetik Anda halal, baik, berkualitas, aman, teruji, dan punya nilai jual yang tinggi.',
                            ],
                            [
                                'Apakah PT ARN mengikuti standar GMP (Good Manufacturing Practice)?',
                                'Iya, perusahaan kami berstandar dan bersertifikasi Good Manufacturing Practice.',
                            ],
                            [
                                'Apa saja sertifikasi yang PT ARN miliki selain GMP?',
                                'Kami telah dan senantiasa memperbarui sertifikasi, HACCP, dan CPKB untuk memenuhi standarisasi usaha secara global.',
                            ],
                            [
                                'Siapa saja klien PT ARN?',
                                'Kami menangani beberapa merek ternama. Untuk informasi lebih lengkap, silahkan cek pada website BPOM <a href="https://cekbpom.pom.go.id/" target="_blank">cekbpom.pom.go.id</a> dengan mengisi "Alga Rosan Nusantara" sebagai nama pendaftar.',
                            ],
                            [
                                'Bisakah saya membuat sampel produk?',
                                'Tentu kami dapat membantu Anda membuat sampel produk. Silahkan hubungi CS kami untuk informasi lebih lanjut.',
                            ],
                            [
                                'Berapa lama estimasi pengerjaan produksi kosmetik di pabrik Anda?',
                                '<p>Perkiraan waktu produksi setelah sampel disetujui, prosesnya sekitar 3-6 bulan. Berikut adalah rincian jangka waktu kerja maklon di tempat kami:</p>
                                <ol>
                                    <li>Pembuatan sampel produk sekitar 7-14 hari kerja.</li>
                                    <li>Pendaftaran HKI 14 hari kerja.</li>
                                    <li>BPOM 18-60 hari kerja, tergantung antrian.</li>
                                    <li>Sertifikasi halal MUI tergantung antrian dan jumlah produk yang didaftarkan.</li>
                                    <li>Produksi 30 hari kerja.</li>
                                </ol>',
                            ],
                            [
                                'Apakah PT ARN membantu proses pendaftaran BPOM, HAKI dan sertifikasi Halal?',
                                'Iya. Kami akan membantu Anda untuk proses registrasi BPOM, HAKI dan sertifikasi Halal MUI.',
                            ],
                            [
                                'Apakah Anda menyediakan wadah dan kemasan?',
                                'Kami memiliki banyak koneksi supplier kemasan dengan berbagai pilihan bentuk dan warna, akan tetapi kami belum menyediakan layanan desain branding secara menyeluruh.',
                            ],
                            [
                                'Bolehkah saya membawa kemasan atau wadah sendiri?',
                                'Boleh. Namun, departemen pengendalian mutu kami akan menguji kualitasnya terlebih dahulu melalui Stability Test. Jika kami menemukan sesuatu yang tidak sesuai, kami akan meminta Anda untuk beralih ke wadah atau kemasan lain. Kami juga dapat memperkenalkan produsen wadah kepada Anda apabila diperlukan.',
                            ],
                            [
                                'Saya tidak terlalu paham tentang dunia kosmetik, bagaimana?',
                                'Tak perlu khawatir. Staf kami berpengalaman dalam memberikan saran produk yang sesuai dengan kebutuhan dan keinginan Anda. Kami juga akan memberikan ide-ide di luar kebutuhan bahan, seperti wadah dan kemasan, pengenalan mitra bisnis yang potensial, dan perancangan konsep produk sesuai dengan target pelanggan Anda. Staf kami berdedikasi untuk memandu Anda dalam berbagai aspek mulai dari manufaktur, perizinan dan legalitas.',
                            ],
                            [
                                'Bolehkah saya mengunjungi kantor dan pabrik Anda?',
                                'Tentu boleh, namun untuk kunjungan diharuskan untuk menginfokan kepada Customer Service kami melalui WhatsApp atau Telepon maksimal H-2 sebelum hari kunjungan.',
                            ],
                            [
                                'Berapa Minimum Order Quantity (MOQ) Anda?',
                                'Untuk MOQ (Minimum of Quantity) tiap produk saat ini dimulai dari 1.000 pieces. Sebagai informasi, MOQ akan menyesuaikan dengan volume tiap produk, jika semakin kecil makan MOQ nya akan semakin besar pula.',
                            ],
                            [
                                'Bisakah Anda membuat produk yang persis dengan yang kami gunakan saat ini di lini bisnis kami?',
                                'Untuk orisinalitas, kami menyarankan pembaruan dengan sedikit modifikasi bahan daripada hanya membuat "produk yang persis sama". Setiap produk yang kami buat memiliki keunikan tersendiri dan dibuat oleh tim Research and Development kami yang sudah ahli di dunia kosmetik.',
                            ],
                            [
                                'Di mana saja produk Anda dijual dan dipasarkan?',
                                'Produk klien kami dijual secara daring (online) maupun luring (offline).',
                            ],
                            [
                                'Bagaimana saya bisa memesan layanan Anda?',
                                'Anda dapat menghubungi tim Customer Service kami di halaman kontak kami.',
                            ],
                            [
                                'Dimana alamat PT ARN?',
                                'Gading Fajar 1 Blok A1 no. 21, Buduran, Sidoarjo - Jatim',
                            ],
                            [
                                'Bagaimana cara bekerjasama dengan PT ARN sebagai supplier kemasan atau bahan baku?',
                                'Silahkan hubungi tim CS kami di halaman kontak',
                            ],

                        ];

                        foreach ($data as $id => [$question, $answer]) {
                            $open = $id == 0;

                        ?>
                            <div class="card">
                                <div class="card-header <?= $open ? 'active' : '' ?>" id="heading<?= $id ?>">
                                    <h3 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?= $id ?>" aria-expanded="<?= $open ? 'true' : 'false' ?>" aria-controls="collapse<?= $id ?>">
                                            <?= $question ?>
                                        </button>
                                    </h3>
                                </div>

                                <div id="collapse<?= $id ?>" class="collapse <?= $open ? 'show' : '' ?>" aria-labelledby="heading<?= $id ?>" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <?= $answer ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End FAQ -->